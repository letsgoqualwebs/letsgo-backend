import {Category, EventRequirements} from "../../app/Models";
import connectDB from '../../config/database';

const CommonSeeder = async () => {
    try {
        await connectDB();
        await CategorySeeder();
        await EventTypeSeeder();
    } catch (e) {
        console.log(e);
    }
};

const CategorySeeder = async () => {
    await Category.deleteMany({});
    const data = [
        {
            name: 'Adventure & Activities',
            attributes : [
                "Organisation" ,"Venue" ,"Price","Host"
            ]
        },
        {
            name: 'Concert & Shows',
            attributes : [
                "Organisation" ,"Venue" ,"Price","Host"
            ]
        },
        {
            name: 'Tours & Attractions',
            attributes : [
                "Organisation" ,"Venue" ,"Price","Host"
            ]
        },
        {
            name: 'Restaurant & Coffee shops',
            attributes : [
                "Organisation" ,"Venue" ,"Price","Host"
            ]
        },
        {
            name: 'Sports & Tournaments',
            attributes : [
                "Organisation" ,"Venue" ,"Price","Host"
            ]
        }
    ];

    await Category.insertMany(data);
};

const EventTypeSeeder = async () => {
    await EventRequirements.deleteMany({});
    const data = [
        {
            name: 'Wedding',
            requirements : [
                "Music" ,"Decoration" ,"Hotel"
            ]
        },
        {
            name: 'Concert & Shows',
            requirements : [
                "Organisation" ,"Venue" ,"Price","Host"
            ]
        },
        {
            name: 'Tours & Attractions',
            requirements : [
                "Organisation" ,"Venue" ,"Price","Host"
            ]
        },
        {
            name: 'Sports & Tournaments',
            requirements : [
                "Organisation" ,"Venue" ,"Price","Host"
            ]
        }
    ];

    await EventRequirements.insertMany(data);
};

CommonSeeder().then(() => {
    process.exit(0);
});
