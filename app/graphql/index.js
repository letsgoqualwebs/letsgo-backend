import { ApolloServer } from 'apollo-server-express';
import schema from './schema';
import jwt from 'jsonwebtoken';
import { User } from '../Models';

const apolloServer = new ApolloServer({
	schema,
	playground: 'development',
	context: async ({ req }) => {
		if(req.body.query.match("Cart_items") || req.body.query.match("UserBookings") ||
			req.body.query.match("Experiences") || req.body.query.match("ProviderBookings")
		|| req.body.query.match("User") || req.body.query.match("UpdateBusiness") || req.body.query.match("addToCart") || req.body.query.match("removeFromCart")
			|| req.body.query.match("singleEvent")
			|| req.body.query.match("providerEvents")
            || req.body.query.match("getSingleProvider")
			|| req.body.query.match("get_experience")) {
			if (req.headers && req.headers.authorization) {
				var auth = req.headers.authorization;
				var parts = auth.split(" ");
				var bearer = parts[0];
				var token = parts[1];
				if (bearer == "Bearer") {
					const user = await getUser(token);
					if (user.error) {
						return  "" ;
					} else return { user };
				} else {
					return "" ;
				}
			} else {
				return  "" ;
			}
		}
		else if (!req.body.query.match("login") && !req.body.query.match("createUser") && !req.body.query.match("createBusiness")
			&& !req.body.query.match("generateOtp") && !req.body.query.match("verifyOtp") && !req.body.query.match("verifyEmailOtp") && !req.body.query.match("generateEmailOtp")
			&& !req.body.query.match("Home_experiences")
			&& !req.body.query.match("Cities")
			&& !req.body.query.match("listingDateAvailability")
			&& !req.body.query.match("Get_experience")
			&& !req.body.query.match("business")
			&& !req.body.query.match("getExperienceCities")
			&& !req.body.query.match("SocialLogins")
			&& !req.body.query.match("listingAvailability")
			&& !req.body.query.match("New_experiences")
			&& !req.body.query.match("Exclusive_experiences")
			&& !req.body.query.match("Popular_experiences")
			&& !req.body.query.match("EventTypes")
			&& !req.body.query.match("EventRequirements")
			&& !req.body.query.match("singleUserEvent")
			&&  !req.body.query.match("forgotPassword")
			&&  !req.body.query.match("users")
		) {

			if (req.headers && req.headers.authorization) {
				var auth = req.headers.authorization;
				var parts = auth.split(" ");
				var bearer = parts[0];
				var token = parts[1];
				if (bearer == "Bearer") {
					const user = await getUser(token);
					if (user.error) {
						throw Error(user.msg);
					} else return { user };
				} else {
					throw Error("Authentication must use Bearer.");
				}
			} else {
				throw Error("User must be authenticated.");
			}
		}
	}
});

const getUser = async (token) => {
	if (token) {
		try {
			// return the user information from the token
			return jwt.verify(token, process.env.JWT_SECRET, async (err, decoded) => {
				return await User.findById(decoded.id);
			} /*process.env.JWT_SECRET*/);
		} catch (err) {
			// if there's a problem with the token, throw an error
			return { error: true, msg: "Session invalid" };
		}
	}
};

export default apolloServer;
