import Events from "../../../Models/User/events";
import {ApolloError} from "apollo-server-errors";
import {EventReview, User} from "../../../Models";
import {eventReviewRatings, experienceReviewRatings, notification} from "../../../Utils/Helpers";
const mongoose = require('mongoose');
const eventQueries = {

    userEvents: async (_, data, context) => {
        try {
            let user = context.user;
            let sort = {createdAt: 'desc'};
            return await Events.find({user_id: user.id, status: data.status}).sort(sort);
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    eventSuggestions: async (_,data) => {
        try {
            var filters = {
                status : 1,
                event_title : new RegExp(data.keyword, 'i'),
            };
            let sort = {createdAt: 'desc'};
            return  await Events.find(filters).sort(sort);
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    allEvents: async (_, data) => {
        try {

            const page = data.page ? data.page - 1 : 0 ;
            const limit = data.page * 5 ;
            const offset = page * 5 ;

            let filter = {
                status : 1,
                event_title : new RegExp(data.keyword, 'i'),
                event_type : data?.category,
                location :data?.location ?  new RegExp(data.location, 'i')  : null,
                is_budget_not_decided : data?.budget ? data?.budget : null,
                is_times_flexible : data?.flexible ? data?.flexible : null
            };

            if (!filter['event_title']) {
                delete filter['event_title']
            }

            if (!filter['event_type']) {
                delete filter['event_type']
            }

            if (!filter['location']) {
                delete filter['location']
            }

            if (!filter['is_budget_not_decided']) {
                delete filter['is_budget_not_decided']
            }

            if (!filter['is_times_flexible']) {
                delete filter['is_times_flexible']
            }

            let sort = {createdAt: 'desc'};

            var events = await Events.find(filter).sort(sort);

            const filtered_events = await eventReviewRatings(events);

            var paginate_events = filtered_events ;

            if (data?.sort_by === 0) {
                paginate_events =  filtered_events
            }
            if (data?.sort_by === 1) {
                paginate_events = await filtered_events.sort(function(a, b) {return b.overall_rate - a.overall_rate;});
            }
            if (data?.sort_by === 2) {
                paginate_events = await filtered_events.sort(function(a, b) {return b.filter_price - a.filter_price});
            }
            if (data?.sort_by === 3) {
                paginate_events = await filtered_events.sort(function(a, b) { return a.filter_price - b.filter_price});
            }
            return { total_page : Math.ceil(paginate_events.length / 5) , events : paginate_events.slice(offset,limit)}

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },


    eventRequestedProviders: async (_, {status, event_id}, context) => {
        try {
            var event = await Events.findOne({_id: new mongoose.Types.ObjectId(event_id)});
            var providers = event.requested_providers && event.requested_providers.length ? event.requested_providers : [];
            return  providers && providers.length ? providers.filter((item) => {
                if (item.request_status === status) {
                    return item
                }
            }) : [];
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },


    providerRequestedEvents: async (_, {status}, context) => {
        try {

            let sort = {createdAt: 'desc'};
            var events = await Events.find().sort(sort);
            var user = context.user;

            var requestedEvents = [] ;
              events && events.length ? events.filter((item) => {
                  item && item.requested_providers?.length ? item.requested_providers.filter((request, index) => {
                    if (
                        request.provider_id === user.id &&
                        request.request_status === status
                    ) {
                        requestedEvents.push(item);
                    }
                }) : [];
            }) : [];

            return  requestedEvents ;

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    providerEvents: async (_, data, context) => {
        try {
            let sort = {createdAt: 'desc'};
            var events = await Events.find().sort(sort);
            var user = context.user;
            var all_Events = [] ;
            const promise = events && events.length ? events.map(async (item) => {
                item['userInfo'] = await User.findOne({'_id' : item.user_id});
                item && item.proposals.length ? item.proposals.filter((proposal) => {
                    if (proposal.provider_id === user.id &&
                        proposal.accepted === data.accepted &&
                        item.status === data.status
                    ) {
                        all_Events.push(item)
                    }
                }) : [];
            }) : [];

            await Promise.all(promise);

            return  all_Events ;

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    successEvent: async (_, data) => {
        try {
            var event = await Events.findOne({_id: new mongoose.Types.ObjectId(data.event_id)});
            event && event.proposals.length ? event.proposals.map((proposal, index) => {
                if (proposal.provider_id !== data.provider_id) {
                    event.proposals.splice(index, 1);
                }
            }) : [];
            return event;
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    singleEvent: async (_, data, context) => {
        var event = await Events.findOne({_id: new mongoose.Types.ObjectId(data.id)});
        var user = context.user;
        var proposals = event && event.proposals.length ? event.proposals : [];
        user && proposals.length ? proposals.map((proposal, index) => {
            if (proposal.provider_id !== user.id) {
                proposals.splice(index, 1);
            }
        }) : event.proposals = [];

        return event;
    },

    singleUserEvent: async (_, data) => {
        return  await Events.findOne({_id: new mongoose.Types.ObjectId(data.id)});
    },

    singleProposal: async (_, data) => {
        var event = await Events.findOne({_id: new mongoose.Types.ObjectId(data.event_id)});
        var proposals = event && event.proposals.length ? event.proposals : [];
        proposals.length ? proposals.map((proposal, index) => {
            if (proposal.provider_id !== data.provider_id) {
                proposals.splice(index, 1);
            }
        }) : event.proposals = [];

        return event.proposals.length ? event.proposals[0] : {}
    },

    getUserEventPayRequest: async (_, data, context) => {

        var milestone_details = {};
        var event = await Events.findOne({_id: new mongoose.Types.ObjectId(data.data.event_id)});
        var proposals = event && event.proposals.length ? event.proposals : [];
        proposals.length ? proposals.map((proposal, index) => {
            if (proposal.provider_id === data.data?.request_by) {
                const price_array = [];
                proposal.milestones && proposal.milestones.length ? proposal.milestones.map(item => {
                    if (item.paid === true) {
                        price_array.push(item['price']);
                    }
                }) : "";
                const total_paid_amount = price_array.length ? price_array.reduce((a, b) => a + b, 0) : 0;
                var quotation_amount = proposal['total_paid_amount'];
                var outstanding_amount = proposal.accepted === true ? proposal.total_paid_amount - (proposal.pre_paid_amount + total_paid_amount) : proposal.total_paid_amount;
                var milestones = proposal.milestones && proposal.milestones.length ? proposal.milestones.filter(function (milestone, i) {
                    return data.data.request_status === null ? milestone : milestone.paid === data?.data?.request_status;
                }) : [];
                milestone_details = {
                    outstanding_amount: outstanding_amount, quotation_amount: quotation_amount,
                    milestones: milestones
                };

                return milestone_details;
            }
        }) : event.proposals = [];

        return milestone_details;
    },

    getMilestonePrice: async (_, data) => {
        var milestone_price = 0;
        var event = await Events.findOne({_id: new mongoose.Types.ObjectId(data.data.event_id)});
        var proposals = event && event.proposals.length ? event.proposals : [];
        proposals.length ? proposals.map((proposal, index) => {
            if (proposal.provider_id === data.data.provider_id) {
                proposal.milestones && proposal.milestones.length ? proposal.milestones.map(item => {
                    if (item.id == data.data.milestone_id) {
                        milestone_price = item['price'];
                        return item.id == data.data.milestone_id;
                    }
                }) : "";
            }
        }) : event.proposals = [];
        var tax = milestone_price * 5 / 100;
        var total = (milestone_price + tax);
        return {price: milestone_price, tax: tax, total_price: total};
    },

    eventAllReviews: async (_, params, context) => {
        let user = context.user;
        var events = await Events.find({"user_id" : user.id });
        var event_ids = events.length ? events.map((event)=>{
            return event._id;
        }) : [] ;
        return  await EventReview.find( { event_id : { $in: event_ids} });
    }

};

export default eventQueries;