import {ApolloError} from 'apollo-server-errors';
import * as yup from "yup";
import {BillingAddress, Cart, Events,EventReview, Experience, User} from './../../../Models';
import mongoose from "mongoose";
import {createOrUpdateChatThread, notification} from "../../../Utils/Helpers";
import moment from "moment";

const createEventSchema = yup.object().shape({

    event_type: yup.string().required({event_type: 'event_type is required'}),
    event_title: yup.string().required({event_title: 'event_title is required'}),
    requirements: yup.array().required("requirements is required"),
    is_dates_flexible: yup.boolean().required({is_dates_flexible: 'is_dates_flexible is required'}),
    start_date: yup.string().when("is_dates_flexible", {
        is: false, then: yup.string().required("start_date is required")
    }),
    end_date: yup.string().when("is_dates_flexible", {
        is: false, then: yup.string().required("end_date is required")
    }),
    is_times_flexible: yup.boolean().required({is_times_flexible: 'is_times_flexible is required'}),
    start_time: yup.string().when("is_times_flexible", {
        is: false, then: yup.string().required("start_time is required")
    }),
    end_time: yup.string().when("is_times_flexible", {
        is: false, then: yup.string().required("end_time is required")
    }),
    is_budget_not_decided: yup.boolean().required({is_budget_not_decided: 'is_budget_not_decided is required'}),
    budget: yup.string().when("is_budget_not_decided", {
        is: false, then: yup.string().required("budget is required")
    }),
    number_of_guest: yup.number().required({number_of_guest: 'number_of_guest is required'}),
    location: yup.string().required({location: 'location is required'}),
});

const addActivitySchema = yup.object().shape({
    name: yup.string().required({name: 'name is required'}),
});

const requestProviderSchema = yup.object().shape({
    provider_id: yup.string().required({ provider_id : "provider_id is required" }),
    event_id: yup.string().required({ event_id : "event_id is required" }),
});

const addActivityInvitationSchema = yup.object().shape({
    event_id: yup.string().required({event_id: 'event_id is required'}),
    activity: yup.string().required({activity: 'activity is required'}),
    schedule: yup.string().required({schedule: 'schedule is required'}),
});

const removeActivitySchema = yup.object().shape({
    id: yup.number().required({id: 'id is required'}),
    event_id: yup.string().required({event_id: 'event_id is required'}),
});

const scheduleActivitySchema = yup.object().shape({
    event_id: yup.string().required({event_id: 'event_id is required'}),
    activity_id: yup.number().required({activity_id: 'activity_id is required'}),
    details: yup.string().required({details: 'activity_id is required'}),
    date: yup.string().required({date: 'date is required'}),
    time: yup.string().required({time: 'time is required'}),
})

const addEventProposalSchema = yup.object().shape({
    event_id: yup.string().required({event_id: 'event_id is required'}),
    message: yup.string().required({message: 'message is required'}),
    valid_till: yup.string().required({valid_till: 'valid_till is required'}),
    quotations: yup.array().of(yup.object().shape({
        activity: yup.string().required('activity is required'),
        is_disabled: yup.boolean().required('is_disabled is required'),
        price: yup.number().required("price is required"),
    })),
    pre_paid_amount: yup.number().required({pre_paid_amount: 'pre_paid_amount is required'}),
});

const addProposalMilestoneSchema = yup.object().shape({
    event_id: yup.string().required({event_id: 'event_id is required'}),
    price: yup.number().required({price: 'price is required'}),
    request_for: yup.string().required({request_for: 'request_for is required'}),
});

const acceptEventSchema = yup.object().shape({
    event_id: yup.string().required({event_id: 'event_id is required'}),
    provider_id: yup.string().required({provider_id: 'provider_id is required'}),
    booking_details: yup.object().shape({
        first_name: yup.string().required('first_name is required'),
        last_name: yup.string().required("last_name is required"),
        email: yup.string().required("email is required"),
        contact_number: yup.string().required("contact_number is required"),
        region: yup.string().required("region is required"),
        post_code: yup.string().required("post_code is required"),
        address: yup.string().required("address is required"),
        latitude: yup.string().required("latitude is required"),
        longitude: yup.string().required("longitude is required"),
    })
});

const milestoneSchema = yup.object().shape({
    milestone_id: yup.string().required({milestone_id: 'milestone_id is required'}),
    event_id: yup.string().required({event_id: 'event_id is required'}),
    provider_id: yup.string().required({provider_id: 'provider_id is required'}),
    booking_details: yup.object().shape({
        first_name: yup.string().required('first_name is required'),
        last_name: yup.string().required("last_name is required"),
        email: yup.string().required("email is required"),
        contact_number: yup.string().required("contact_number is required"),
        region: yup.string().required("region is required"),
        post_code: yup.string().required("post_code is required"),
        address: yup.string().required("address is required"),
        latitude: yup.string().required("latitude is required"),
        longitude: yup.string().required("longitude is required"),
    })
});


const eventMutations = {

    requestProviderOnEvent: async (_, {provider_id,event_id}, context) => {
        try {
            let user = context.user;
            if (user.role === "provider"){
                return new ApolloError("You are not authorized for this request");
            }
            await requestProviderSchema.validate({provider_id : provider_id,event_id : event_id}, {abortEarly: false});
            var event = await Events.findOne({event_id : event_id});
            var providers = event?.requested_providers ? event?.requested_providers : [] ;
            var availability =  providers.length ? providers.filter(x => JSON.stringify(x._id) === JSON.stringify(provider_id)) : [];
            if (availability?.length > 0){
                return new ApolloError("already requested for this provider")
            }else{
                const provider = await User.findOne({_id : provider_id});
                let provider_data = provider.toJSON();
                provider_data['provider_id'] = provider_id;
                provider_data['request_status'] = 0;
                providers.push(provider_data);
            }

           return  await Events.findOneAndUpdate({
                _id: new mongoose.Types.ObjectId(event_id),
            }, {
                $set: {
                    requested_providers: providers
                }
            }, {returnOriginal: false});

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    createEvent: async (_, {data}, context) => {
        try {
            let user = context.user;
            await createEventSchema.validate(data, {abortEarly: false});
            const activities = [];
            data.requirements ? data.requirements.map((requirement, index) => {
                activities.push({'id': index + 1, 'activity': requirement, 'schedules': [],status : 0})
            }) : [];

            const newEvent = new Events(data);
            newEvent.user_id = user.id
            newEvent.status = 1;
            newEvent.activities = activities
            newEvent.proposals = []
            newEvent.save();
            return newEvent;

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    editEvent: async (_, {event})=> {
        try{
            await createEventSchema.validate(event, {abortEarly: false});
            const activities = [];
            event.requirements ? event.requirements.map(requirement => {
                activities.push(requirement)
            }) : [];

            return await Events.findOneAndUpdate({_id: event.id}, {
                $set: {
                    event_type: event.event_type,
                    event_title: event.event_title,
                    requirements: activities,
                    start_date: event.start_date,
                    end_date: event.end_date,
                    end_time: event.end_time,
                    start_time: event.start_time,
                    is_dates_flexible:event.is_dates_flexible,
                    is_times_flexible: event.is_times_flexible,
                    number_of_guest: event.number_of_guest,
                    location: event.location,
                    budget: event.budget,
                    is_budget_not_decided: event.is_budget_not_decided
                },
            },{returnOriginal: false});
        }
        catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    cancelEvent: async (_,data, context) => {
        try {
            let user = context.user;
            var event = await Events.findOne({_id: new mongoose.Types.ObjectId(data.id)});
            if (user._id !== event.user_id) {
                 await Events.findOneAndUpdate({_id: new mongoose.Types.ObjectId(data.id)}, {
                    $set: {
                        status: 2,
                    },
                }, {returnOriginal: false});
            }
            return await Events.findOne({_id: new mongoose.Types.ObjectId(data.id)});;

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    completeEvent: async (_,data, context) => {
        try {
            let user = context.user;
            var event = await Events.findOne({_id: new mongoose.Types.ObjectId(data.id)});

            if (JSON.stringify(user._id) === JSON.stringify(event.user_id)) {
                await Events.findOneAndUpdate({_id: new mongoose.Types.ObjectId(data.id)}, {
                    $set: {
                        status: 3,
                    },
                }, {returnOriginal: false});
            }
            return await Events.findOne({_id: new mongoose.Types.ObjectId(data.id)});;

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    completeEventActivityEvent: async (_,{id,event_id}, context) => {
        try {
            let user = context.user;
            var event = await Events.findOne({_id: event_id});
            if (event && JSON.stringify(user._id) === JSON.stringify(event.user_id)) {
                var index = event.activities.findIndex(value => value.id === id);
                await Events.findOneAndUpdate({
                    _id: event_id,
                }, {
                    $set: {
                        [`activities.${index}`]: {
                            id : event.activities[index].id,
                            activity: event.activities[index].activity,
                            schedules: event.activities[index].schedules,
                            status : 1
                        }
                    }
                }, {returnOriginal: false});
            }
            return await Events.findOne({_id: event_id });
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    addActivityToEvent: async (_, data, context) => {
        try {
            let user = context.user;
            await addActivitySchema.validate(data, {abortEarly: false});
            const event = await Events.findOne({_id: new mongoose.Types.ObjectId(data.id)});
            const activity_array = event && event.activities.length > 0 ? event.activities : [];
            const ids = activity_array.length ? activity_array.map(object => {
                return object.id;
            }) : [];
            const max = ids.length ? Math.max(...ids) : 0;
            activity_array.push({'id': max + 1, 'activity': data.name, 'schedules': [], status : 0 });
            var new_activity = await Events.findOneAndUpdate({_id: new mongoose.Types.ObjectId(data.id)}, {
                $set: {
                    activities: activity_array,
                },
            }, {returnOriginal: false});

            return new_activity;

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },


    addEventProposals: async (_, {data}, context) => {
        try {

            let user = context.user;

            if(user.role === "provider") {

                await addEventProposalSchema.validate(data, {abortEarly: false});
                const event = await Events.findOne({_id: new mongoose.Types.ObjectId(data.event_id)});
                const proposal_array = event && event.proposals.length > 0 ? event.proposals : [];

                //if event is requested from user
                var event_providers = event?.requested_providers ? event?.requested_providers : [] ;
                var availability =  event_providers.length ? event_providers.filter(x => JSON.stringify(x._id) === JSON.stringify(user._id)) : [];
                if (availability && availability.length > 0) {
                    var provider_array_index = proposal_array.map(x => {
                        return JSON.stringify(x._id);
                    }).indexOf(JSON.stringify(user._id));
                    var  provider_index = provider_array_index === -1 ? null : provider_array_index;
                    if(provider_index) {
                        var provider = await User.findOne({_id: user._id}) ;
                        let provider_data = provider.toJSON();
                        provider_data['provider_id'] = JSON.stringify(user._id);
                        provider_data['request_status'] = 0;
                        await Events.findOneAndUpdate({
                            _id: new mongoose.Types.ObjectId(data.event_id),
                        }, {
                            $set: {
                                [`requested_providers.${provider_array_index}`]: {
                                    provider_data
                                }
                            }
                        }, {returnOriginal: false});
                    }
                }

                var index = null;
                if (proposal_array.length > 0) {
                    var array_index = proposal_array.map(x => {
                        return x.provider_id;
                    }).indexOf(user.id);
                    index = array_index === -1 ? null : array_index;
                }
                const price_array = [];

                data.quotations.length ? data.quotations.map(item => {
                    price_array.push(item['price']);
                }) : "";
                const total_paid_amount = price_array.length ? price_array.reduce((a, b) => a + b, 0) : 0;
                const thread = await createOrUpdateChatThread(event.user_id,user.id);

                if (total_paid_amount !== 0) {

                    if (total_paid_amount > data.pre_paid_amount) {

                        if (index === null) {

                            proposal_array.push({
                                message: data.message,
                                valid_till: data.valid_till,
                                provider_id: user.id,
                                provider: {
                                    profile_picture:  user && user.profile_image ? user.profile_image : "https://upay.qualwebs.com/petecation/public/storage/letsgo/1646119043pexels-jeshootscom-4.jpeg",
                                    business_name: user && user.business ? user.business.business_name : "",
                                    rating: 0,
                                    experience_images: ["https://upay.qualwebs.com/petecation/public/storage/letsgo/1646119043pexels-jeshootscom-4.jpeg"]
                                },
                                quotations: data.quotations,
                                milestones : [],
                                pre_paid_amount: data.pre_paid_amount,
                                total_paid_amount: total_paid_amount,
                                accepted : false,
                                chat_thread_id : thread._id,
                                created_at : moment().valueOf()
                            });

                            //find and update
                            const updated = await Events.findOneAndUpdate({
                                _id: new mongoose.Types.ObjectId(data.event_id),
                            }, {
                                $set: {
                                    proposals: proposal_array
                                }
                            }, {returnOriginal: false});

                            //push notification
                            await notification(event?.user_id,"event",'Events',"New proposal on event "+event?.event_title,data.event_id);

                            return updated ;

                        } else {

                            const updated =  await Events.findOneAndUpdate({
                                _id: new mongoose.Types.ObjectId(data.event_id),
                            }, {
                                $set: {
                                    [`proposals.${index}`]: {
                                        message: data.message,
                                        valid_till: data.valid_till,
                                        provider_id: user.id,
                                        provider: {
                                            profile_picture: user && user.profile_image ? user.profile_image : "https://upay.qualwebs.com/petecation/public/storage/letsgo/1646119043pexels-jeshootscom-4.jpeg",
                                            business_name: user && user.business ? user.business.business_name : "",
                                            rating: 0,
                                            experience_images: ["https://upay.qualwebs.com/petecation/public/storage/letsgo/1646119043pexels-jeshootscom-4.jpeg"]
                                        },
                                        quotations: data.quotations,
                                        pre_paid_amount: data.pre_paid_amount,
                                        total_paid_amount: total_paid_amount,
                                        accepted : false,
                                        chat_thread_id : thread._id,
                                        created_at : moment().valueOf()
                                    }
                                }
                            }, {returnOriginal: false});

                            //push notification
                            await notification(event?.user_id,"event",'Events',"New proposal on event "+event?.event_title,data.event_id);

                            return  updated ;
                        }
                    } else {
                        return new ApolloError('Quotation pre amount is not correct');
                    }
                } else {
                    return new ApolloError('Please add price with activity to continue');
                }
            }else {
                return new ApolloError('Un-authorized');
            }
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    addProposalMilestone: async (_, {data}, context) => {
        try {

            let user = context.user;

            await addProposalMilestoneSchema.validate(data, {abortEarly: false});
                const event = await Events.findOne({_id: new mongoose.Types.ObjectId(data.event_id)});
                const proposal_array = event && event.proposals.length > 0 ? event.proposals : [];

                var index = null;
                if (proposal_array.length > 0) {
                    var array_index = proposal_array.map(x => {
                        return x.provider_id;
                    }).indexOf(user.id);
                    index = array_index === -1 ? null : array_index;
                }

                var milestones_array = proposal_array[index] && proposal_array[index].milestones ? proposal_array[index].milestones : [];
                milestones_array.push({ 'id' : moment().valueOf(),'price': data.price, 'request_for': data.request_for,'paid' : false});

                const price_array = [];
                milestones_array.length ? milestones_array.map(item => {
                    price_array.push(item['price']);
                }) : "";
                const total_paid_amount = price_array.length ? price_array.reduce((a, b) => a + b, 0) : 0;

                if ((total_paid_amount + proposal_array[index].pre_paid_amount) <= proposal_array[index].total_paid_amount) {

                return await Events.findOneAndUpdate({
                    _id: new mongoose.Types.ObjectId(data.event_id),
                }, {
                    $set: {
                        [`proposals.${index}.milestones`]: milestones_array,
                    }
                }, {returnOriginal: false});

            }else{
                return new ApolloError(`You have only ${proposal_array[index].total_paid_amount - ((total_paid_amount - data.price) + proposal_array[index].pre_paid_amount) } amount is remains`);
            }
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    eventActivitySchedules: async (_, {data}, context) => {
        try {

            let user = context.user;
            await scheduleActivitySchema.validate(data, {abortEarly: false});
            const event = await Events.findOne({_id: new mongoose.Types.ObjectId(data.event_id)});
            const activity_array = event && event.activities.length > 0 ? event.activities : [];

            if (activity_array.length > 0) {
                var activity = activity_array.find(obj => {
                    return obj.id === data.activity_id
                })
                var schedules = activity && activity.schedules ? activity.schedules : [];
                schedules.push({'details': data.details, 'date': data.date, 'time': data.time});

                var index = activity_array.map(x => {
                    return x.id;
                }).indexOf(data.activity_id);

                return await Events.findOneAndUpdate({
                    _id: new mongoose.Types.ObjectId(data.event_id),
                }, {
                    $set: {
                        [`activities.${index}`]: {
                            id: data.activity_id, activity: activity.activity, schedules: schedules
                        }
                    }
                }, {returnOriginal: false});

            } else {
                return new ApolloError('Please add activity to continue');
            }
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    removeActivityFromEvent: async (_, data, context) => {
        try {
            let user = context.user;
            await removeActivitySchema.validate(data, {abortEarly: false});
            const event = await Events.findOne({_id: new mongoose.Types.ObjectId(data.event_id)});
            const activity_array = event && event.activities.length > 0 ? event.activities : [];

            var index = activity_array.map(x => {
                return x.id;
            }).indexOf(data.id);
            activity_array.splice(index, 1);

            var removed_activity = await Events.findOneAndUpdate({_id: new mongoose.Types.ObjectId(data.event_id)}, {
                $set: {
                    activities: activity_array,
                },
            }, {returnOriginal: false});

            return removed_activity;

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    acceptEventQuotation: async (_, {data}, context) => {
        try {
            let user = context.user;

            await acceptEventSchema.validate(data, {abortEarly: false});
            const event = await Events.findOne({_id: new mongoose.Types.ObjectId(data.event_id)});
            const proposal_array = event && event.proposals.length > 0 ? event.proposals : [];
            var index = proposal_array.map(x => {
                return x.provider_id;
            }).indexOf(data.provider_id);

            let provider_proposal = proposal_array.find(item => item.provider_id === data.provider_id);
            var payment = true ;
            const thread = await createOrUpdateChatThread(event.user_id,user.id); //user.id (provider_id) //logged-in provider
            if (provider_proposal && payment === true) {

            var response = await Events.findOneAndUpdate({
                    _id: new mongoose.Types.ObjectId(data.event_id),
                }, {
                    $set: {
                        [`proposals.${index}`]: {
                            message: provider_proposal.message,
                            valid_till: provider_proposal.valid_till,
                            provider_id: provider_proposal.provider_id,
                            provider: {
                                profile_picture: provider_proposal && provider_proposal.provider ? provider_proposal.provider.profile_picture : "",
                                business_name: provider_proposal && provider_proposal.provider ? provider_proposal.provider.business_name : "",
                                rating: 0,
                                experience_images: ["https://upay.qualwebs.com/petecation/public/storage/letsgo/1646119043pexels-jeshootscom-4.jpeg"]
                            },
                            quotations: provider_proposal.quotations,
                            milestones:  provider_proposal.milestones,
                            pre_paid_amount: provider_proposal.pre_paid_amount,
                            total_paid_amount: provider_proposal.total_paid_amount,
                            accepted : true,
                            chat_thread_id : thread._id,
                            payment_history : [
                                {
                                    request_id : "pre"+new Date().getUTCMilliseconds(),
                                    sub_total : provider_proposal.pre_paid_amount,
                                    tax : provider_proposal.pre_paid_amount * 2 /100,
                                    total_paid_amount : (provider_proposal.pre_paid_amount + provider_proposal.pre_paid_amount * 2 /100),
                                    amount_paid_for : "Advance booking",
                                    payment_card : data.payment_card,
                                    booking_details : data.booking_details
                                }
                            ]
                        }
                    }
                }, {returnOriginal: false});

                //push notification
                await notification(provider_proposal.provider_id,"event",'Events',"Event proposal accepted for"+event?.event_title,data.event_id)

                return  response ;
            }

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    milestonePaymentInput: async (_, {data}, context) => {
        try {

            await milestoneSchema.validate(data, {abortEarly: false});
            const event = await Events.findOne({_id: new mongoose.Types.ObjectId(data.event_id)});

            const proposal_array = event && event.proposals.length > 0 ? event.proposals : [];
            var index = proposal_array.map(x => {
                return x.provider_id;
            }).indexOf(data.provider_id);

            let provider_proposal = proposal_array.find(item => item.provider_id === data.provider_id);
            var payment = true ;

            if (provider_proposal && payment === true) {

                const milestone_array = provider_proposal && provider_proposal.milestones.length > 0 ? provider_proposal.milestones : [];
                let milestone_price = milestone_array.find(item => item.id && item.id == data.milestone_id);
                var milestone_new_array = [] ;
                milestone_array.length ? milestone_array.map(milestone => {
                    milestone_new_array.push({'id' : milestone.id ,'price':milestone.price,'request_for':milestone.request_for,
                        'paid': milestone.id == data.milestone_id ? true : milestone.paid,
                        'paid_at' : moment().valueOf()
                    })
                }) : [];

                var payment_history = provider_proposal.payment_history.length ? provider_proposal.payment_history : [] ;
                payment_history.push(
                    {
                        request_id : "milestone"+milestone_price.id,
                        sub_total : milestone_price.price,
                        tax :  milestone_price.price * 5 /100,
                        total_paid_amount :  milestone_price.price + (milestone_price.price * 5 /100),
                        amount_paid_for : milestone_price.request_for,
                        payment_card : data.payment_card,
                        booking_details : data.booking_details
                    }
                );

                return await Events.findOneAndUpdate({
                    _id: new mongoose.Types.ObjectId(data.event_id),
                }, {
                    $set: {
                        [`proposals.${index}`]: {
                            message: provider_proposal.message,
                            valid_till: provider_proposal.valid_till,
                            provider_id: provider_proposal.provider_id,
                            provider: {
                                profile_picture: provider_proposal && provider_proposal.provider ? provider_proposal.provider.profile_picture : "",
                                business_name: provider_proposal && provider_proposal.provider ? provider_proposal.provider.business_name : "",
                                rating: 0,
                                experience_images: ["https://upay.qualwebs.com/petecation/public/storage/letsgo/1646119043pexels-jeshootscom-4.jpeg"]
                            },
                            quotations: provider_proposal.quotations,
                            milestones:  milestone_new_array,
                            pre_paid_amount: provider_proposal.pre_paid_amount,
                            total_paid_amount: provider_proposal.total_paid_amount,
                            accepted : true,
                            payment_history :payment_history
                        }
                    }
                }, {returnOriginal: false});
            }

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    createEventInvitationGroup: async (_, {data}, context) => {
        try {
            await addProposalMilestoneSchema.validate(data, {abortEarly: false});
            const event = await Events.findOne({_id: new mongoose.Types.ObjectId(data.event_id)});
            const group_array = event && event.invitation_groups && event.invitation_groups.length > 0 ? event.invitation_groups : [];
            const ids = group_array.length ? group_array.map(object => {
                return object.id;
            }) : [];
            const max = ids.length ? Math.max(...ids) : 0;
            group_array.push({'id': max + 1, 'activity': data.activity, 'schedule': data.schedule,'count':0 , 'accepted_invitations' : []});
            var new_activity = await Events.findOneAndUpdate({_id: new mongoose.Types.ObjectId(data.event_id)}, {
                $set: {
                    invitation_groups: group_array,
                },
            }, {returnOriginal: false});

            return new_activity;

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },


    eventRateAndReview: async (_, data, context) => {
        try {
            let user = context.user;
            var available = await EventReview.findOne({"event_id": data.event_id , "user_id" : user.id });
            if(available){
                return await EventReview.findOneAndUpdate({
                    "event_id": data.event_id ,
                    "user_id" : user.id
                }, {
                    $set: {
                        review: data.review,
                        rating: data.rating,
                        user : {
                            id : user.id ,
                            first_name : user.first_name ,
                            last_name : user.last_name ,
                            profile_image : user?.profile_image
                        }
                    },
                }, {returnOriginal: false});
            }else {
                var review = new EventReview(data);
                review.user_id = user.id;
                review.user = {id: user.id, first_name: user.first_name, last_name: user.last_name ,profile_image : user?.profile_image};
                return review.save();
            }
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error);
        }
    },
};

export default eventMutations;
