import {ApolloError} from 'apollo-server-errors';
import * as yup from "yup";
import {Otp, Reviews} from "../../../Models";

const rateReviewSchema = yup.object().shape({
    experience_id: yup.string().required({experience_id: 'experience_id is required'}),
    review: yup.string().required({review: 'review is required'}),
    ratings: yup.array().of(
        yup.object().shape({
            attribute_name: yup.string().required('business_name is required'),
            rating: yup.number().min(1).max(5).required('rating is required'),
        })),
});

const reviewRatingMutations = {

    rateAndReview: async (_, data, context) => {
        await rateReviewSchema.validate(data.review, {abortEarly: false});
        try {
            let user = context.user;
            console.log(user);
            var dataReview = data.review ;
            var available = await Reviews.findOne({"experience_id": data.review.experience_id ,
                "user.id" : user.id });
            if(available){
                return await Reviews.findOneAndUpdate({"experience_id": data.review.experience_id ,
                    "user.id" : user.id
                }, {
                    $set: {
                        review: data.review.review,
                        ratings: data.review.ratings,
                        user : {id : user.id ,first_name : user.first_name , last_name : user.last_name}
                    },
                }, {returnOriginal: false});
            }else {
                var review = "" ;
                review = new Reviews(dataReview);
                review.experience_id = data.review.experience_id
                review.user = {id: user.id, first_name: user.first_name, last_name: user.last_name}
                review.review = data.review.review
                review.ratings = data.review.ratings
                return review.save();
            }

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error);
        }
    },
};

export default reviewRatingMutations;
