import {Reviews} from "../../../Models";
import {ApolloError} from "apollo-server-errors";

const reviewQueries = {

    singleReviewRate: async (_, params, context) => {
        try {
            let user = context.user;
            console.log(params.experience_id,user.id);
            return await Reviews.findOne({experience_id: params.experience_id ,
            "user.id" :user.id});
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },
}

export default reviewQueries;