import {Category, EventRequirements} from "../../../../Models";

const categoryQueries = {

  categories: async (_, args) => {
    return await Category.find();
  },

  cities: async (_, args) => {
   return [
     {
       "city": "Riyadh",
       "lat": "24.6500",
       "lng": "46.7100",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "Ar Riyāḑ",
       "capital": "primary",
       "population": "6881000",
       "population_proper": "6694000",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659343508767.jpeg"
     },
     {
       "city": "Jeddah",
       "lat": "21.5428",
       "lng": "39.1728",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "Makkah al Mukarramah",
       "capital": "",
       "population": "3976000",
       "population_proper": "3976000",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659343550100.jpeg"
     },
     {
       "city": "Mecca",
       "lat": "21.4225",
       "lng": "39.8261",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "Makkah al Mukarramah",
       "capital": "admin",
       "population": "1675368",
       "population_proper": "1675368",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659343638978.jpeg"
     },
     {
       "city": "Medina",
       "lat": "24.4667",
       "lng": "39.6000",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "Al Madīnah al Munawwarah",
       "capital": "admin",
       "population": "1180770",
       "population_proper": "1180770",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659343584438.jpeg"
     },
     {
       "city": "Ad Dammām",
       "lat": "26.4333",
       "lng": "50.1000",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "Ash Sharqīyah",
       "capital": "admin",
       "population": "903312",
       "population_proper": "903312",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659343668762.jpeg"
     },
     {
       "city": "Al Hufūf",
       "lat": "25.3608",
       "lng": "49.5997",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "Ash Sharqīyah",
       "capital": "",
       "population": "637389",
       "population_proper": "400000",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659343697080.jpeg"
     },
     {
       "city": "Buraydah",
       "lat": "26.3333",
       "lng": "43.9667",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "Al Qaşīm",
       "capital": "admin",
       "population": "614093",
       "population_proper": "614093",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659343725024.jpeg"
     },
     {
       "city": "Al Ḩillah",
       "lat": "23.4895",
       "lng": "46.7564",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "Ar Riyāḑ",
       "capital": "",
       "population": "594605",
       "population_proper": "594605",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659343764286.jpeg"
     },
     {
       "city": "Aţ Ţā’if",
       "lat": "21.2667",
       "lng": "40.4167",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "Makkah al Mukarramah",
       "capital": "",
       "population": "579970",
       "population_proper": "579970",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659343797112.jpeg"
     },
     {
       "city": "Tabūk",
       "lat": "28.3838",
       "lng": "36.5550",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "Tabūk",
       "capital": "admin",
       "population": "547957",
       "population_proper": "455450",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659343871749.jpeg"
     },
     {
       "city": "Khamīs Mushayţ",
       "lat": "18.3000",
       "lng": "42.7333",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "‘Asīr",
       "capital": "",
       "population": "430828",
       "population_proper": "430828",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659343902923.jpeg"
     },
     {
       "city": "Ḩā’il",
       "lat": "27.5236",
       "lng": "41.7001",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "Ḩā’il",
       "capital": "admin",
       "population": "385257",
       "population_proper": "385257",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659343946113.jpeg"
     },
     {
       "city": "Al Qaţīf",
       "lat": "26.5196",
       "lng": "50.0115",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "Ash Sharqīyah",
       "capital": "",
       "population": "368892",
       "population_proper": "98259",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659343982052.jpeg"
     },
     {
       "city": "Al Mubarraz",
       "lat": "25.4100",
       "lng": "49.5808",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "Ash Sharqīyah",
       "capital": "",
       "population": "298562",
       "population_proper": "290802",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659344024139.jpeg"
     },
     {
       "city": "Al Kharj",
       "lat": "24.1556",
       "lng": "47.3120",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "Ar Riyāḑ",
       "capital": "",
       "population": "298428",
       "population_proper": "298428",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659344057098.jpeg"
     },
     {
       "city": "Najrān",
       "lat": "17.4917",
       "lng": "44.1322",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "Najrān",
       "capital": "admin",
       "population": "298288",
       "population_proper": "298288",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659344086496.jpeg"
     },
     {
       "city": "Yanbu‘",
       "lat": "24.0943",
       "lng": "38.0493",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "Al Madīnah al Munawwarah",
       "capital": "",
       "population": "267590",
       "population_proper": "200161",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659344114719.png"
     },
     {
       "city": "Abhā",
       "lat": "18.2167",
       "lng": "42.5000",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "‘Asīr",
       "capital": "admin",
       "population": "236157",
       "population_proper": "236157",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659344140518.jpeg"
     },
     {
       "city": "Arar",
       "lat": "30.9833",
       "lng": "41.0167",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "Al Ḩudūd ash Shamālīyah",
       "capital": "admin",
       "population": "167057",
       "population_proper": "167057",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659344167114.jpeg"
     },
     {
       "city": "Jāzān",
       "lat": "16.8892",
       "lng": "42.5611",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "Jāzān",
       "capital": "admin",
       "population": "134764",
       "population_proper": "134764",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659344195143.jpeg"
     },
     {
       "city": "Sakākā",
       "lat": "30.0000",
       "lng": "40.1333",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "Al Jawf",
       "capital": "admin",
       "population": "128332",
       "population_proper": "128332",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659344220787.jpeg"
     },
     {
       "city": "Al Bāḩah",
       "lat": "20.0129",
       "lng": "41.4677",
       "country": "Saudi Arabia",
       "iso2": "SA",
       "admin_name": "Al Bāḩah",
       "capital": "admin",
       "population": "95089",
       "population_proper": "95089",
       "images":"https://letsgo-content.s3.ap-southeast-1.amazonaws.com/dev/1659344257567.jpeg"
     }
   ];
  },

  category: async (_, {id}) => {
    return await Category.findById(id);
  },

  categoryAttributes: async (_, {name}) => {
    return await Category.findOne({name : name});
  },

  eventRequirements: async (_, {id}) => {
    return await EventRequirements.findOne({id : id});
  },

  eventTypes: async (_, args) => {
    return await EventRequirements.find();
  },

};

export default categoryQueries;
