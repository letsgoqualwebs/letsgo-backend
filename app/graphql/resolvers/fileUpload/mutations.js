import {S3} from 'aws-sdk'
import {
    ApolloError
} from 'apollo-server-express';


const uploadMutation = {
    singleUpload: async (_, {file}) => {

        try {

            return new Promise(async (resolve, reject) => {
                if (!file) {
                    return new ApolloError('Please select a file to upload')
                }
                const filename = await file?.file?.filename;
                const mimetype = await file?.file?.mimetype;
                const createReadStream = await file?.file?.createReadStream;
                let stream = createReadStream();

                // AWS S3 Credentials
                const s3 = new S3({
                    region: process.env.AWS_REGION,
                    accessKeyId: process.env.AWS_ACCESS_ID,
                    secretAccessKey: process.env.AWS_SECRET_KEY
                });

                const params = {
                    Bucket: process.env.AWS_S3_BUCKET,
                    Key: 'dev/' + filename,
                    Body: stream,
                    ACL: 'public-read'
                };

                s3.upload(params).promise().then((res) => {
                    resolve({mimetype: mimetype, location: res.Location});
                });
            });
        } catch (err) {
            throw new ApolloError(err.message);
        }
    },

    base64Upload: async (_, {file}) => {
        try {
            return new Promise(async (resolve, reject) => {

                // AWS S3 Credentials
                const s3 = new S3({
                    region: process.env.AWS_REGION,
                    accessKeyId: process.env.AWS_ACCESS_ID,
                    secretAccessKey: process.env.AWS_SECRET_KEY
                });

                const buf = Buffer.from(file.replace(/^data:image\/\w+;base64,/, ""), 'base64');
                var data = {
                    Key: `dev/${Date.now()}.${file.split(';')[0].split('/')[1]}`,
                    Body: buf,
                    Bucket: process.env.AWS_S3_BUCKET,
                    ContentEncoding: 'base64',
                    ContentType: 'image/png',
                    ACL: 'public-read'
                };

                s3.upload(data).promise().then((res) => {
                    resolve({location: res.Location});
                });
            });
        } catch (err) {
            throw new ApolloError(err.message);
        }
    },

    multipleUpload: async (_, files) => {

        try {

            var urls = [];

            files?.files.length ? files?.files.map((file) => {

                // AWS S3 Credentials
                const s3 = new S3({
                    region: process.env.AWS_REGION,
                    accessKeyId: process.env.AWS_ACCESS_ID,
                    secretAccessKey: process.env.AWS_SECRET_KEY
                });

                const buf = Buffer.from(file.replace(/^data:image\/\w+;base64,/, ""), 'base64');

                var data = {
                    Key: `dev/${Date.now()}.${file.split(';')[0].split('/')[1]}`,
                    Body: buf,
                    Bucket: process.env.AWS_S3_BUCKET,
                    ContentEncoding: 'base64',
                    ContentType: 'image/png',
                    ACL: 'public-read'
                };

                const url = s3.upload(data).promise().then((res) => {
                    return res.Location
                });
                urls.push(url);

            }) : "";

          return  {location: urls};

        } catch (err) {
            throw new ApolloError(err.message);
        }
    },
};

export default uploadMutation;
