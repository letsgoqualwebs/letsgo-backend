import {ApolloError} from 'apollo-server-errors';
import * as yup from "yup";
import {Experience} from './../../../Models'
import mongoose from "mongoose";
import experience from "../../../Models/User/experience";
import {experienceReviewRatings} from "../../../Utils/Helpers";

const createExperienceSchema = yup.object().shape({
    category: yup.string().required({category: 'category is required'}),
    available_for: yup.array().required({available_for: 'available_for is required'}),
    title: yup.string().required({title: 'title is required'}),
    days: yup.number().required({days: 'days is required'}),
    overview: yup.string().required({overview: 'overview is required'}),
    inclusion: yup.string().required({inclusion: 'inclusion is required'}),
    exclusion: yup.string().required({exclusion: 'exclusion is required'}),
    schedule_info: yup.string().required({schedule_info: 'schedule_info is required'}),
    booking_inf: yup.string().required({booking_inf: 'booking_inf is required'}),
    is_online: yup.boolean().required({is_online: 'is_online is required'}),
    location: yup.string().when("is_online", {
        is: false, then: yup.string().required("location is required")
    }),
    city: yup.string().when("is_online", {
        is: false, then: yup.string().required("city is required")
    }),
    latitude: yup.string().when("is_online", {
        is: false, then: yup.string().required("longitude is required")
    }),
    longitude: yup.string().when("is_online", {
        is: false, then: yup.string().required("longitude is required")
    }),
});

const packageExperienceSchema = yup.array().of(
    yup.object().shape({
        package_title: yup.string().required({package_title: 'package_title is required'}),
        package_id: yup.number().required({package_id: 'package_id is required'}),
        package_info: yup.string().required({package_info: 'package_info is required'}),
        package_include: yup.string().required({package_include: 'package_include is required'}),
        is_free_package: yup.boolean().required({is_free_package: 'is_free_package is required'}),
        package_group: yup.array().of(
            yup.object().shape({
                price: yup.number().required('price is required'),
                group: yup.string().required("group is required"),
                people_per_group: yup.string().required("people_per_group is required"),
            })),
    })
);

const availabilitySchema = yup.array().of(
    yup.object().shape({
        start_time: yup.string().required({start_time: 'start_time is required'}),
        end_time: yup.string().required({end_time: 'end_time is required'}),
        number_of_booking: yup.number().required({number_of_booking: 'number_of_booking is required'}),
        days: yup.array().required("days is required"),
        unavailable_dates: yup.array().required("unavailable_dates is required"),
        available_till_date: yup.date().required("available_till_date is required"),
    })
);

const scheduleExperienceSchema = yup.array().of(
    yup.object().shape({
        day: yup.number().required({day: 'day is required'}),
        schedules: yup.array().of(
            yup.object().shape({
                time: yup.string().required('time is required'),
                activity: yup.string().required("activity is required"),
            })),
    })
);

const refundExperienceSchema = yup.object().shape({
    is_refundable: yup.boolean().required({is_refundable: 'is_refundable is required'}),
    policies: yup.array().of(
        yup.object().shape({
            percentage: yup.number().required({percentage: 'percentage is required'}),
            reservation_days: yup.number().required({reservation_days: 'reservation_days is required'}),
        })
    ),
});

const galleryExperienceSchema = yup.object({
    images: yup.array().min(1, "at least 1").required("required")
});

const experienceMutations = {

    createExperience: async (_, {experience}, context) => {
        try {
            await createExperienceSchema.validate(experience, {abortEarly: false});
            let user = context.user;
            const newUser = new Experience();
            newUser.experience_details = experience;
            newUser.business_id = user._id;
            newUser.is_published = 0;
            newUser.exclusive = false;
            newUser.experience_pages_completed = [1];
            newUser.save();
            return newUser;
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },


    updateExperience: async (_, {experience}, context) => {
        try {
            await createExperienceSchema.validate(experience, {abortEarly: false});
            let user = context.user;
            let experience_id = experience.experience_id ;
            delete experience.experience_id ;

            await Experience.findOneAndUpdate({_id: new mongoose.Types.ObjectId(experience_id)}, {
                $set: {
                    experience_details: experience,
                    business_id: user._id
                },
            }, {returnOriginal: false});

            return await Experience.findOne({_id: new mongoose.Types.ObjectId(experience_id)});

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },


    experiencePackage: async (_, {experience}, context) => {
        try {
            await packageExperienceSchema.validate(experience.packages, {abortEarly: false});
            var experience_id = experience.experience_id;
            delete experience.experience_id;
            var experience_details = await Experience.findOne({_id: new mongoose.Types.ObjectId(experience_id)});
            var pages = experience_details?.experience_pages_completed?.length ? experience_details?.experience_pages_completed : [];
            pages.push(2);
            var page_array = Array.from(new Set(pages));
            let user = context.user;
            await Experience.findOneAndUpdate({_id: new mongoose.Types.ObjectId(experience_id)}, {
                $set: {
                    experience_package: experience.packages,
                    business_id: user._id,
                    experience_pages_completed :page_array,
                    is_published : page_array.length === 6 ? 1 : 0
                },
            }, {returnOriginal: false});

            return await Experience.findOne({_id: new mongoose.Types.ObjectId(experience_id)});

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    experienceAvailability: async (_, {experience}, context) => {
        try {
            await availabilitySchema.validate(experience.slots, {abortEarly: false});
            var experience_id = experience.experience_id;
            delete experience.experience_id;
            let user = context.user;

            var experience_details = await Experience.findOne({_id: new mongoose.Types.ObjectId(experience_id)});
            var pages = experience_details?.experience_pages_completed?.length ? experience_details?.experience_pages_completed : [];
            pages.push(3);
            var page_array = Array.from(new Set(pages));

            var new_experience = await Experience.findOneAndUpdate({_id: new mongoose.Types.ObjectId(experience_id)}, {
                $set: {
                    experience_availability: experience.slots,
                    business_id: user._id,
                    experience_pages_completed :page_array,
                    is_published : page_array.length === 6 ? 1 : 0
                },
            }, {returnOriginal: true});

            return new_experience;
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    experienceSchedule: async (_, {experience}, context) => {
        try {
            await scheduleExperienceSchema.validate(experience.activities, {abortEarly: false});
            var experience_id = experience.experience_id;
            delete experience.experience_id;
            let user = context.user;

            var experience_details = await Experience.findOne({_id: new mongoose.Types.ObjectId(experience_id)});
            var pages = experience_details?.experience_pages_completed?.length ? experience_details?.experience_pages_completed : [];
            pages.push(4);
            var page_array = Array.from(new Set(pages));

            var new_experience = await Experience.findOneAndUpdate({_id: new mongoose.Types.ObjectId(experience_id)}, {
                $set: {
                    experience_schedule: experience.activities,
                    business_id: user._id,
                    experience_pages_completed :page_array,
                    is_published : page_array.length === 6 ? 1 : 0
                },
            }, {returnOriginal: false});

            return new_experience;

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },


    experienceRefund: async (_, {experience}, context) => {
        try {
            await refundExperienceSchema.validate(experience, {abortEarly: false});
            var experience_id = experience.experience_id;
            delete experience.experience_id;
            let user = context.user;

            var experience_details = await Experience.findOne({_id: new mongoose.Types.ObjectId(experience_id)});
            var pages = experience_details?.experience_pages_completed?.length ? experience_details?.experience_pages_completed : [];
            pages.push(5);
            var page_array = Array.from(new Set(pages));

            var new_experience = await Experience.findOneAndUpdate({_id: new mongoose.Types.ObjectId(experience_id)}, {
                $set: {
                    experience_refund_policy: experience,
                    business_id: user._id,
                    experience_pages_completed :page_array,
                    is_published : page_array.length === 6 ? 1 : 0
                },
            }, {returnOriginal: false});

            return new_experience;
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    experienceGallery: async (_, {experience}, context) => {
        try {
            await galleryExperienceSchema.validate(experience, {abortEarly: false});
            var experience_id = experience.experience_id;
            delete experience.experience_id;
            let user = context.user;

            var experience_details = await Experience.findOne({_id: new mongoose.Types.ObjectId(experience_id)});
            var pages = experience_details?.experience_pages_completed?.length ? experience_details?.experience_pages_completed : [];
            pages.push(6);
            var page_array = Array.from(new Set(pages));

            var new_experience = await Experience.findOneAndUpdate({_id: new mongoose.Types.ObjectId(experience_id)}, {
                $set: {
                    experience_gallery: experience.images,
                    business_id: user._id,
                    experience_pages_completed :page_array,
                    is_published : page_array.length === 6 ? 1 : 0
                },
            }, {returnOriginal: false});

            return new_experience;

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    searchExperiences: async (_, {data}) => {

        try {

            const page = data.page ? data.page - 1 : 0 ;
            const limit = data.page * 10 ;
            const offset = page * 10 ;

            var dayObject = "";
            if (data.available_days && data.available_days.length > 0) {
                let days = data.available_days.indexOf(4) !== -1;
                if (days) {
                    var length =  Array.from({ length: 100 - 5 + 1 }, (_, i) => 5 + i);
                    var merge_length = data.available_days.concat(length);
                    dayObject = {$in: merge_length}
                } else {
                    dayObject = {$in: data.available_days}
                }
            }

            let price = data.is_free_package;
            let start_discount = data.start_discount ;
            let end_discount = data.end_discount ;

            var filters = {
                'experience_details.title': new RegExp(data.title, 'i'),
                'experience_details.city': new RegExp(data.city, 'i'),
                'experience_details.days': dayObject,
                'experience_details.category': {$in: data.categories},
                'experience_details.available_for': { $all : data.available_for},
                is_published: 1,
                status: true
            };

            if (price === true) {
                filters['experience_package.is_free_package'] = true;
            } else {
                filters['experience_package.0.package_group.0.price'] = {$gte: data.min_price, $lte: data.max_price};
                filters['experience_package.is_free_package'] = false;
            }

            if (start_discount !== "" && end_discount !== "") {
                filters['experience_refund_policy.policies.percentage'] = {$gte: start_discount, $lte: end_discount};
            }

            if (!data.title) {
                delete filters['experience_details.title']
            }

            if (!data.city) {
                delete filters['experience_details.city']
            }

            if (!filters['experience_details.days'] || data.available_days.length < 1) {
                delete filters['experience_details.days']
            }

            if (!filters['experience_details.category'] || data.categories.length < 1) {
                delete filters['experience_details.category']
            }

            if (!filters['experience_details.available_for'] || data.available_for.length < 1) {
                delete filters['experience_details.available_for']
            }

            let sort = {createdAt: 'desc'};

            const experiences = await experience.find(filters).sort(sort);

            const filtered_experience = await experienceReviewRatings(experiences);

            var paginate_experiences = filtered_experience ;

            if (data.sort_by === 0) {
                paginate_experiences =  filtered_experience ;
            }
            if (data.sort_by === 1) {
                paginate_experiences = filtered_experience.sort(function(a, b) {return b.overall_rate - a.overall_rate;});
            }
            if (data.sort_by === 2) {
                paginate_experiences = filtered_experience.sort(function(a, b) {return b.experience_package[0].package_group[0].price - a.experience_package[0].package_group[0].price;});
            }
            if (data.sort_by === 3) {
                paginate_experiences = filtered_experience.sort(function(a, b) {return a.experience_package[0].package_group[0].price - b.experience_package[0].package_group[0].price;});
            }

            return { total_page : Math.ceil(paginate_experiences.length / 10) , experiences : paginate_experiences.slice(offset,limit)}

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    changeExperienceStatus: async (_,data, context) => {
        try {
            return await Experience.findOneAndUpdate({_id: new mongoose.Types.ObjectId(data.experience_id)}, {
                $set: {
                    status : data.status,
                },
            }, {returnOriginal: false});
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },
};

export default experienceMutations;
