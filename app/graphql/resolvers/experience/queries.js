import experience from "../../../Models/User/experience";
import {ApolloError} from "apollo-server-errors";
import moment from "moment";
import {experienceReviewRatings, getDaysBetweenDates} from '../../../Utils/Helpers';
import {Booking, EventReview, Events, Experience, Reviews} from "../../../Models";

const mongoose = require('mongoose');

const experienceQueries = {

    experiences: async (_, params, context) => {
        try {
            var status = params.status;
            let user = context.user;

            var filters = {
                business_id: user.id,
            };
            if (status === 2) {
                filters['status'] = false
            } else {
                filters['is_published'] = status
                filters['status'] = true
            }

            var experiences = await experience.find(filters);
            var bookings = await Booking.find();

            experiences.map(async (experience_item, key) => {

                var upcoming_bookings = 0;
                var total_bookings = 0;

                bookings && bookings.length ? bookings.map((item) => {
                    item && item.order_items.length ? item.order_items.map(async (booking, key) => {
                        if (booking.package_experience_id === experience_item.id) {
                            upcoming_bookings = upcoming_bookings + 1;
                            total_bookings = total_bookings + 1;
                        }
                    }) : [];
                }) : [];

                experience_item['upcoming_bookings'] = upcoming_bookings;
                experience_item['total_bookings'] = total_bookings;
            });


            experiences.length ? experiences.sort((x, y) => Number(moment(y.createdAt).valueOf()) - Number(moment(x.createdAt).valueOf())) : experiences;
            return experiences;

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    new_experiences: async (_, args) => {
        try {
            let sort = {createdAt: 'desc'};
            var experiences = await experience.find({is_published: 1}).sort(sort);
            return experienceReviewRatings(experiences);
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    exclusive_experiences: async (_, args) => {
        try {
            let sort = {createdAt: 'desc'};
            const experiences = await experience.find({exclusive: true, is_published: 1}).sort(sort);
            return experienceReviewRatings(experiences);
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    popular_experiences: async (_, args) => {
        try {
            let sort = {total_bookings: 'desc'};
            var bookings = await Booking.find();
            var experiences = await experience.find({is_published: 1}).sort(sort);

            experiences = experienceReviewRatings(experiences);

            experiences.length ? experiences.map(async (experience_item, key) => {
                var total_bookings = 0;
                bookings && bookings.length ? bookings.map((item) => {
                    item && item.order_items.length ? item.order_items.map(async (booking, key) => {
                        if (booking.package_experience_id === experience_item.id) {
                            total_bookings = total_bookings + 1;
                        }
                    }) : [];
                }) : [];
                experience_item['total_bookings'] = total_bookings;
            }) : [];

            return experiences.sort((a, b) => b.total_bookings - a.total_bookings);

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    home_experiences: async (_, args) => {
        try {
            return await experience.find({is_published: 1});
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    experienceSuggestions: async (_, data) => {
        try {
            var filters = {
                'experience_details.title': new RegExp(data.keyword, 'i'),
                is_published: 1,
                status: true
            };
            let sort = {createdAt: 'desc'};
            return  await experience.find(filters).sort(sort);
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    get_experience: async (_, id, context) => {

        try {
            var user = context.user;
            var experiences = await experience.findOne({_id: new mongoose.Types.ObjectId(id)});
            var experience_rating = [];
            var ratings = await Reviews.find({experience_id: new mongoose.Types.ObjectId(id)});
            ratings.map((item, key) => {
                var total_ratings = [];
                item.ratings ? item.ratings.map((rate, key) => {
                    total_ratings.push(rate.rating)
                }) : [];
                item.overall_rate = total_ratings.length > 0 ? (total_ratings.reduce((a, b) => a + b, 0) / total_ratings.length).toFixed(1) : 0;
                total_ratings.length > 0 ? experience_rating.push(total_ratings.reduce((a, b) => a + b, 0) / total_ratings.length) : 0;
            });
            experiences.ratings = ratings;
            experiences.overall_rate = experience_rating.length > 0 ? (experience_rating.reduce((a, b) => a + b, 0) / experience_rating.length).toFixed(1) : 0;

            let upcoming_bookings = 0;
            let total_bookings = 0;
            let total_earning = 0 ;

            var bookings = await Booking.find();
            var allow_rate = false;
            bookings && bookings.length ? bookings.map((item) => {
                if (item.user_id === (user ? user.id : "")) {
                    item && item.order_items.length ? item.order_items.map(async (booking, key) => {
                        if (booking.package_experience_id === id.id) {
                            allow_rate = true;
                            return true;
                        }
                    }) : []
                }
                item && item.order_items.length ? item.order_items.map(async (booking, key) => {
                    if (booking.package_experience_id === id.id) {
                            upcoming_bookings = upcoming_bookings + 1;
                            total_bookings = total_bookings + 1;
                            total_earning = total_earning + item.total_paid_amount;
                    }
                }) : [];
            }) : [];


            experiences['upcoming_bookings'] = upcoming_bookings;
            experiences['total_bookings'] = total_bookings;
            experiences['total_earning'] = parseFloat(total_earning).toFixed(2);
            experiences.allow_rate = allow_rate;
            return experiences;
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    similarExperiences: async (_, {id}) => {
        try {
            var currentExperience = await experience.findOne({_id: id});
            var filters = {
                '_id': {$ne: new mongoose.Types.ObjectId(id)},
                'experience_details.category': currentExperience.experience_details.category,
                is_published: 1,
            };
            const experiences = await experience.find(filters);
            return await experienceReviewRatings(experiences);

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    singleExperiencePackage: async (_, data) => {
        const experiences = await experience.findOne({_id: new mongoose.Types.ObjectId(data.package_experience_id)});
        const packages = experiences ? experiences.experience_package : [];
        var package_detail = null;
        packages.map((item, key) => {
            if (item.package_id === data.package_id) {
                package_detail = item;
            }
        });
        return package_detail
    },

    listingAvailability: async (_, params) => {

        //Available requirements
        var month = params.month;
        var listing = await experience.findOne({_id: new mongoose.Types.ObjectId(params.id)});
        var experience_availability = listing.experience_availability;
        const current_month = moment().format('M');
        const request_month = moment(month).format('M');


        //Managing available days
        var available_days = [];
        var unavailable_dates = [];
        var max_till_date = [];
        experience_availability.map(item => {
            item.days.map(day => {
                available_days.push(day);
            });
            unavailable_dates.push(item.unavailable_dates);
            max_till_date.push(item.available_till_date);
        });

        var mxDate = max_till_date.reduce(function (a, b) {
            return a > b ? a : b;
        });

        const unique_days = [...new Set(available_days)];
        const slot_unavailable_dates = unavailable_dates.length ? unavailable_dates.shift().filter(function (v) {
            return unavailable_dates.every(function (a) {
                return a.indexOf(v) !== -1;
            });
        }) : [];

        const unique_unavailable_dates = slot_unavailable_dates.length ? Array.from(new Set(slot_unavailable_dates)) : [];

        //Managing date range
        var startOfMonth = null;
        var endOfMonth = null;

        if (current_month === request_month) {
            startOfMonth = moment().format('YYYY-MM-DD');
            endOfMonth = moment().endOf('month').format('YYYY-MM-DD');
        } else {
            startOfMonth = moment(month).startOf('month').format('YYYY-MM-DD');
            endOfMonth = moment(month).endOf('month').format('YYYY-MM-DD');
        }
        var date_ranges = getDaysBetweenDates(moment(startOfMonth.toString()), moment(endOfMonth.toString()));


        //Manage available dates
        var available_dates = [];
        date_ranges.map(date => {
            if (moment(date).valueOf() <= moment(mxDate).valueOf()) {
                const day = moment(date).format('dddd');

                const available = unique_days.includes(day.toLowerCase());
                const un_available = unique_unavailable_dates.indexOf(date.toLowerCase());
                if (available === true && un_available === -1) {
                    available_dates.push(date);
                }
            }
        });

        return {dates: available_dates};
    },


    listingDateAvailability: async (_, params) => {

        var listing = await experience.findOne({_id: new mongoose.Types.ObjectId(params.id)});
        var experience_availability = listing ? listing.experience_availability : [];

        //Available slots
        var available_slots = [];
        if (experience_availability && experience_availability.length > 0) {
            experience_availability.map(item => {
                const day = moment(params.request_date).format('dddd');
                const available = item.days.includes(day.toLowerCase());
                const unavailable = item.unavailable_dates.indexOf(params.request_date.toLowerCase());
                if (available === true && unavailable === -1) {
                    if (moment(params.request_date).valueOf() <= moment(item.available_till_date).valueOf()) {
                        available_slots.push({start_time: item.start_time, end_time: item.end_time});
                    }
                }
            });
        }
        return {slots: available_slots};
    },


    getExperienceCities: async (_, args) => {
        const experiences = await experience.find();
        var cities = [];
        experiences.length ? experiences.map(item => {
            var city = item.experience_details ? item.experience_details.city : "";
            if (city) {
                cities.push(city);
            }
        }) : [];

        let unique_cities = [...new Set(cities)];

        return unique_cities;
    },

    experienceAllReviews: async (_, params, context) => {
        let user = context.user;
        var experiences = await Experience.find({"business_id" : user.id });
        var experience_ids = experiences.length ? experiences.map((experience)=>{
            return experience._id;
        }) : [] ;
        return  await Reviews.find( { experience_id : { $in: experience_ids} });
    }
};

export default experienceQueries;