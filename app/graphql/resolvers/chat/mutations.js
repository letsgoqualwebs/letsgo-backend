import {ApolloError} from 'apollo-server-errors';
import * as yup from "yup";
import {Cart, ChatLine, ChatThread, User, UserNotification} from "../../../Models";
import mongoose from "mongoose";
import {
    createOrUpdateChatThread,
    notification,
    pushFirebaseChatMessages,
    updateFirebaseChatThread
} from "../../../Utils/Helpers";

const admin = require("firebase-admin");
var db = admin.database();

const createTheadSchema = yup.object().shape({
    user_id: yup.string().required({user_id: 'user_id is required'}),
});

const sendMessageSchema = yup.object().shape({
    chat_thread_id: yup.string().required({chat_thread_id: 'chat_thread_id is required'}),
    message: yup.string(),
    documents: yup.array().optional(),
});

const chatMutations = {
    createChatThread: async (_, data, context) => {
        try {
            let user = context.user;
            await createTheadSchema.validate(data.data, {abortEarly: false});
            return await createOrUpdateChatThread(data.data.user_id,user.id);
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error);
        }
    },

    updateNotificationReadStatus: async (_, params, context) => {
        let user = context.user;
        await UserNotification.updateMany({user_id: user?.id},
            {
                $set: {
                    read_status: true,
                }
            }, {returnOriginal: false});

        return "updated";
    },

    updateThreadReadStatus: async (_, {chat_thread_id,provider_id}, context) => {
        try {

            let user = context.user;
            const ref = db.ref('chat_thread');
            ref.child(chat_thread_id).child(provider_id).set({ unread_count : 0});

            if (user.role === "user"){
                await ChatThread.findOneAndUpdate({
                        _id: new mongoose.Types.ObjectId(chat_thread_id),
                    },
                    {
                        $set: {
                            provider_unread_count: 0,
                        }
                    }, {returnOriginal: false});
            }

            if (user.role === "provider"){
                await ChatThread.findOneAndUpdate({
                        _id: new mongoose.Types.ObjectId(chat_thread_id),
                    },
                    {
                        $set: {
                            user_unread_count: 0,
                        }
                    }, {returnOriginal: false});
            }

           return await ChatThread.findById(chat_thread_id);

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error);
        }
    },


    sendMessage: async (_, data, context) => {
        try {
            let user = context.user ;
            await sendMessageSchema.validate(data.data, {abortEarly: false});
            var thread_message = await ChatLine.findOne({'chat_thread_id' : new mongoose.Types.ObjectId(data.data.chat_thread_id)});
            var messages = thread_message && thread_message.messages.length ? thread_message.messages : [];
            var message_id = Date.now().valueOf() ;
            var send_timestamp = Date.now().valueOf() ;
            var thread = await ChatThread.findOne({'_id':new mongoose.Types.ObjectId(data.data.chat_thread_id)})

           //Append Message to thread messages
            messages.push({
                id : message_id,
                sender_id : user.id,
                message : data.data.message,
                documents : data.data.documents ? data.data.documents : [],
                sender : {
                    id: user.id,
                    first_name: user.first_name,
                    last_name: user.last_name,
                    profile_image: user.profile_image ? user.profile_image : "http://54.151.160.83/petecation/public//images/profile.png"
                },
                has_document : data.data.documents && data.data.documents.length ? true : false,
                send_timestamp : send_timestamp,
                read_status : false
            })

            await updateOrCreateMessage(messages,data.data.chat_thread_id);


            //Push message to firebase
            pushFirebaseChatMessages({
                id : message_id,
                sender_id : user.id,
                message : data.data.message,
                documents : data.data.documents ? data.data.documents : [],
                sender :  {
                    id: user.id,
                    first_name: user.first_name,
                    last_name: user.last_name,
                    profile_image: user.profile_image ? user.profile_image : "http://54.151.160.83/petecation/public//images/profile.png"
                },
                has_document : data.data.documents && data.data.documents.length ? true : false,
                send_timestamp : send_timestamp,
                read_status : false
            },data.data.chat_thread_id,message_id);

            await notification(user.id === thread?.user_id ? thread?.provider_id : thread?.user_id ,"chat",'Chat',data.data.message,data.data.chat_thread_id)

            //update last on thread
            await updateFirebaseChatThread(data.data.chat_thread_id,user.id,data.data.message)

            return  await ChatLine.findOne({'chat_thread_id' : new mongoose.Types.ObjectId(data.data.chat_thread_id)});
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error);
        }
    },
};


async function updateOrCreateMessage(messages,chat_thread_id) {

    var data = {
        chat_thread_id : chat_thread_id,
        thread: await ChatThread.findOne({'_id': new mongoose.Types.ObjectId(chat_thread_id)}),
        messages : messages,
   }

    return ChatLine.findOne({chat_thread_id : chat_thread_id,})
        .then(function (obj) {
            // update
            if (obj) {
                return obj.updateOne(data);
            }
            return ChatLine.create(data);
        })
}

export default chatMutations;
