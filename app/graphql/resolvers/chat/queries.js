import {ApolloError} from "apollo-server-errors";
import {ChatThread, ChatLine} from "../../../Models";
import mongoose from "mongoose";

const chatQueries = {

    getUserThreads: async (_, data, context) => {

        try {
            let user = context.user;
            var threads = [] ;

            if (user.role === "user"){
                 threads = await ChatThread.find({'user_id': new mongoose.Types.ObjectId(user.id)});
            }
            if (user.role === "provider"){
                 threads = await ChatThread.find({'provider_id': new mongoose.Types.ObjectId(user.id)});
            }
            var userThreads = [] ;

            threads && threads.length ? threads.map(async (thread, key) => {

                if (thread.user_id !== user.id){
                    userThreads.push({
                        id : thread.id,
                        unread_count : thread.user_unread_count ? thread.user_unread_count : 0 ,
                        last_message : null,
                        last_timestamp : null,
                        user : {
                            id : thread.user_id,
                            first_name : thread.user ? thread.user.first_name : "",
                            last_name: thread.user ? thread.user.last_name : "",
                            profile_image : thread.user && thread.user.profile_image ? thread.user.profile_image : "http://54.151.160.83/petecation/public//images/profile.png",
                        },
                    });
                }

                if (thread.provider_id !== user.id ){
                    userThreads.push({
                        id : thread.id,
                        unread_count : thread.provider_unread_count ? thread.provider_unread_count : 0 ,
                        last_message : null,
                        last_timestamp : null,
                        user : {
                            id : thread.provider_id,
                            first_name : thread.provider ? thread.provider.first_name : "",
                            last_name: thread.provider ? thread.provider.last_name : "",
                            profile_image : thread.provider && thread.provider.profile_image ? thread.provider.profile_image : "http://54.151.160.83/petecation/public//images/profile.png",
                        }
                    });
                }

            }) : [];

            return userThreads ;

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },


    getThreadMessages: async (_, data, context) => {
        try {
            return   await ChatLine.findOne({'chat_thread_id': new mongoose.Types.ObjectId(data.id)});
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    }
};

export default chatQueries;
