import {ApolloError} from "apollo-server-errors";
import {BillingAddress, Booking} from "../../../Models";
import mongoose from "mongoose";
import experience from "../../../Models/User/experience";
import moment from 'moment';
import {getDaysBetweenDates} from "../../../Utils/Helpers";

const bookingQueries = {

    providerBookings: async (_, data, context) => {

        try {

            var bookings = [];
            let user = context.user;

            if (data?.date) {
                var start = new Date(data?.date);
                start.setHours(0, 0, 0, 0);

                var end = new Date(data?.date);
                end.setHours(23, 59, 59, 999);
            }

            var filters = {
                status: data.status,
                createdAt: data?.date ? {$gte: start, $lt: end} : ""
            };

            if (!filters['status']) {
                delete filters['status']
            }

            if (!filters['createdAt']) {
                delete filters['createdAt']
            }

            if (user.role === "provider") {
                bookings = await Booking.find(filters);
                var users = [];
                bookings && bookings.length ? bookings.map((item) => {
                    var booked_usr = item.booking_details ? item.booking_details : "";
                    var userObject = "";

                    item && item.order_items.length ? item.order_items.map(async (booking, key) => {
                        if (booking.package_experience_id === data['experience_id'])
                            if (booked_usr) {
                                var package_tickets = [];
                                booking && booking.package_groups.length ? booking.package_groups.map((group, key) => {
                                    package_tickets.push(group.number_of_tickets);
                                }) : [];

                                userObject = {
                                    order_id: item._id,
                                    first_name: booked_usr.first_name,
                                    last_name: booked_usr.last_name,
                                    contact_number: booked_usr.contact_number,
                                    email: booked_usr.email,
                                    total_booked_tickets: package_tickets.reduce((a, b) => a + b, 0),
                                    createdAt: item.createdAt,
                                    start_date : booking.start_date,
                                    start_time: booking.start_time,
                                    end_time: booking.end_time
                                };
                                users.push(userObject);
                            }
                    }) : []
                }) : [];

                return users;
            }
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    allBookings: async () => {
        try {
            return await Booking.find();
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    dashboardStatistics: async (_, params, context) => {
        try {

            let user = context.user;
            var filters = {
                business_id: user.id,
                is_published: true,
                status: true
            };
            var users = [];
            var experiences = await experience.find(filters);
            var bookings = await Booking.find({status: 1});
            var total_earnings = [];
            var total_bookings = 0;

            experiences.map(async (experience_item) => {

                bookings && bookings.length ? bookings.map(async (item) => {
                    var booked_usr = item.booking_details ? item.booking_details : "";
                    item && item.order_items.length ? item.order_items.map(async (booking) => {
                        if (booking.package_experience_id === experience_item.id) {

                            var package_tickets = [];
                            booking && booking.package_groups.length ? booking.package_groups.map((group) => {
                                package_tickets.push(group.number_of_tickets);
                            }) : [];

                            var userObject = {
                                order_id: item._id,
                                first_name: booked_usr.first_name,
                                last_name: booked_usr.last_name,
                                contact_number: booked_usr.contact_number,
                                email: booked_usr.email,
                                total_booked_tickets: package_tickets.reduce((a, b) => a + b, 0),
                                createdAt: item.createdAt,
                                start_time: booking.start_time,
                                end_time: booking.end_time
                            };
                            users.push(userObject);
                            total_bookings = total_bookings + 1;
                            total_earnings.push(parseInt(item.total_paid_amount));
                        }
                    }) : [];
                }) : [];
            });

            return {
                total_earning: total_earnings.reduce((a, b) => a + b, 0),
                total_bookings: total_bookings,
                total_experience: experiences.length,
                upcoming_bookings: users
            }
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },


    salesGraphStatistics: async (_, params, context) => {
        try {

            let user = context.user;
            var filters = {
                business_id: user.id,
                is_published: true,
                status: true
            };


            if (params.type === "week" || params.type === "month") {

                var bookings_array = [];
                var startDateRange = moment(params.start_date).format('YYYY-MM-DD');
                var endDateRange = moment(params.end_date).format('YYYY-MM-DD');
                var dateRanges = getDaysBetweenDates(moment(startDateRange.toString()), moment(endDateRange.toString()));
                console.log(dateRanges);

                //mapping events with date and repetition
                const promise = dateRanges.map(async (date) => {
                    var experiences = await experience.find(filters);
                    var bookings = await Booking.find({
                        status: 1,
                        createdAt: {
                            $gte: new Date(moment(date).format('YYYY-MM-DD')),
                            $lt: new Date(moment(date).add(1, 'day').format('YYYY-MM-DD'))
                        }
                    });
                    var total_bookings = 0;
                    experiences.map(async (experience_item) => {
                        bookings && bookings.length ? bookings.map(async (item) => {
                            item && item.order_items.length ? item.order_items.map(async (booking) => {
                                if (booking.package_experience_id === experience_item.id) {
                                    total_bookings = total_bookings + 1;
                                }
                            }) : [];
                        }) : [];
                    });
                    bookings_array.push({date : moment(date).format('MMMM Do'),value: moment(date).format('dddd'), bookings: total_bookings});
                });

                //return with promise
                await Promise.all(promise);

            } else if (params.type === "year") {

                var bookings_array = [];
                const months = moment.months();
                const promise = months.map(async (month) => {

                    const changeMonth = params.year + "/" + moment().month(month).format("M") + "/" + "01";

                    //managing date range
                    var startOfMonth = null;
                    var endOfMonth = null;

                    if (moment().format('M') === moment(changeMonth).format('M')) {
                        startOfMonth = moment().format('YYYY-MM-DD');
                        endOfMonth = moment().endOf('month').format('YYYY-MM-DD');
                    } else {
                        startOfMonth = moment(changeMonth).startOf('month').format('YYYY-MM-DD');
                        endOfMonth = moment(changeMonth).endOf('month').format('YYYY-MM-DD');
                    }

                    var experiences = await experience.find(filters);
                    var bookings = await Booking.find({
                        status: 1,
                        createdAt: {
                            $gte: new Date(moment(startOfMonth).format('YYYY-MM-DD')),
                            $lt: new Date(moment(endOfMonth).add(1, 'day').format('YYYY-MM-DD'))
                        }
                    });
                    var total_bookings = 0;
                    experiences.map(async (experience_item) => {
                        bookings && bookings.length ? bookings.map(async (item) => {
                            item && item.order_items.length ? item.order_items.map(async (booking) => {
                                if (booking.package_experience_id === experience_item.id) {
                                    total_bookings = total_bookings + 1;
                                }
                            }) : [];
                        }) : [];
                    });

                    bookings_array.push({date : "",value: month, bookings: total_bookings});

                });

                //return with promise
                await Promise.all(promise);
            }

            return {
                total_bookings: bookings_array,
            }

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },


    earningGraphStatistics: async (_, params, context) => {
        try {

            let user = context.user;
            var filters = {
                business_id: user.id,
                is_published: true,
                status: true
            };
            var bookings_array = [];

            if (params.type === "week" || params.type === "month") {

                var startDateRange = moment(params.start_date).format('YYYY-MM-DD');
                var endDateRange = moment(params.end_date).format('YYYY-MM-DD');
                var dateRanges = getDaysBetweenDates(moment(startDateRange.toString()), moment(endDateRange.toString()));

                //mapping events with date and repetition
                const promise = dateRanges.map(async (date) => {
                    var total_earnings = [];
                    var experiences = await experience.find(filters);
                    var bookings = await Booking.find({
                        status: 1,
                        createdAt: {
                            $gte: new Date(moment(date).format('YYYY-MM-DD')),
                            $lt: new Date(moment(date).add(1, 'day').format('YYYY-MM-DD'))
                        }
                    });
                    experiences.map(async (experience_item) => {
                        bookings && bookings.length ? bookings.map(async (item) => {
                            total_earnings.push(parseInt(item.total_paid_amount));
                        }) : [];
                    });
                    bookings_array.push({ date : moment(date).format('MMMM Do'), value: moment(date).format('dddd'), amount: total_earnings.reduce((a, b) => a + b, 0)});
                });

                //return with promise
                await Promise.all(promise);

            } else if (params.type === "year") {

                const months = moment.months();
                const promise = months.map(async (month) => {

                    const changeMonth = params.year + "/" + moment().month(month).format("M") + "/" + "01";

                    //managing date range
                    var total_earnings = [] ;
                    var startOfMonth = null;
                    var endOfMonth = null;

                    if (moment().format('M') === moment(changeMonth).format('M')) {
                        startOfMonth = moment().format('YYYY-MM-DD');
                        endOfMonth = moment().endOf('month').format('YYYY-MM-DD');
                    } else {
                        startOfMonth = moment(changeMonth).startOf('month').format('YYYY-MM-DD');
                        endOfMonth = moment(changeMonth).endOf('month').format('YYYY-MM-DD');
                    }

                    var experiences = await experience.find(filters);
                    var bookings = await Booking.find({
                        status: 1,
                        createdAt: {
                            $gte: new Date(moment(startOfMonth).format('YYYY-MM-DD')),
                            $lt: new Date(moment(endOfMonth).add(1, 'day').format('YYYY-MM-DD'))
                        }
                    });

                    experiences.map(async (experience_item) => {
                        bookings && bookings.length ? bookings.map(async (item) => {
                            total_earnings.push(parseInt(item.total_paid_amount));
                        }) : [];
                    });

                    bookings_array.push({ date : "", value: month,amount: total_earnings.reduce((a, b) => a + b, 0)});
                });

                //return with promise
                await Promise.all(promise);
            }

            return {
                total_earnings: bookings_array,
            }

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },


    userBookings: async (_, data, context) => {
        try {

            let user = context.user;
            var bookings = [];

            if (user.role === "user") {
                if (data.status === 1) {
                    bookings = await Booking.find(
                        {
                            'user_id': user._id,
                            'status': 1,
                            // 'order_items': {
                            //     "end_date": {
                            //         "$gte": moment().toDate()
                            //     }
                            // }
                        }).sort({createdAt: 'desc'});
                } else if (data.status === 2) {
                    bookings = await Booking.find({
                            $or: [
                                {'user_id': user._id,'status': 2},
                                // {
                                //     'order_items': {
                                //         "end_date": {
                                //             "$lte": moment().toDate()
                                //         }
                                //     }
                                //  }
                             ]
                        }
                    ).sort({createdAt: 'desc'});
                } else if (data.status === 3) {
                    bookings = await Booking.find({
                        'user_id': user._id,
                        'status': 3
                    }).sort({createdAt: 'desc'});
                } else {
                    bookings = await Booking.find({'user_id': user._id}).sort({createdAt: 'desc'});
                }
            }

            var booking_items = [];
            bookings && bookings.length ? bookings.map(async (item) => {

                item && item.order_items.length ? item.order_items.map(async (booking, key) => {

                    var package_tickets = [];
                    booking && booking.package_groups.length ? booking.package_groups.map((group, key) => {
                        package_tickets.push(group.number_of_tickets);
                    }) : [];

                    booking['order_id'] = item._id;
                    booking['business_provider'] = booking.business_provider ? booking.business_provider : "";
                    booking['business_location'] = booking.business_location ? booking.business_location : "";
                    booking['business_category'] = booking.business_category ? booking.business_category : "";
                    booking['number_of_tickets'] = package_tickets.reduce((a, b) => a + b, 0);
                    booking_items.push(booking)
                }) : [];
            }) : [];

            return booking_items;

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    singleOrder: async (_, ID, context) => {
        try {
            var bookings = await Booking.findOne({'_id': new mongoose.Types.ObjectId(ID.id)});

            bookings && bookings.order_items.length ? bookings.order_items.map(async (item, key) => {

                var package_tickets = [];
                item && item.package_groups.length ? item.package_groups.map(async (group, key) => {
                    package_tickets.push(group.number_of_tickets);
                }) : [];
                item['total_experience_tickets'] = package_tickets.reduce((a, b) => a + b, 0);
            }) : [];

            return bookings;

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },


    getBillingAddress: async (_, args, context) => {
        try {
            let user = context.user;
            return await BillingAddress.findOne({'user_id': user.id});
        } catch
            (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

};

export default bookingQueries;
