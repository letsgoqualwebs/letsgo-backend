import {ApolloError} from 'apollo-server-errors';
import * as yup from "yup";
import {BillingAddress, Booking, Cart, User} from "../../../Models";
import {maskify} from "../../../Utils/Helpers";

const placeOrderSchema = yup.object().shape({
    cart_unique_id: yup.string().required({cart_unique_id: 'cart_unique_id is required'}),
    payment_card_id: yup.string().required({payment_card_id: 'payment_card_id is required'}),
    booking_details: yup.object().shape({
        first_name: yup.string().required('first_name is required'),
        last_name: yup.string().required("last_name is required"),
        email: yup.string().required("email is required"),
        contact_number: yup.string().required("contact_number is required"),
        region: yup.string().required("region is required"),
        post_code: yup.string().required("post_code is required"),
        address: yup.string().required("address is required"),
        latitude: yup.string().required("latitude is required"),
        longitude: yup.string().required("longitude is required"),
    })
});

const bookingMutations = {

    placeAnOrder: async (_, data, context) => {
        try {

            const booking_data = data.data;
            await placeOrderSchema.validate(booking_data, {abortEarly: false});
            let user = context.user;
            var order = null ;

            const cart = await Cart.findOne({'cart_unique_id': booking_data.cart_unique_id});

            if (cart && cart.cart_items.length > 0) {
                order = new Booking();
                order.user_id = user._id;
                order.booking_details = booking_data.booking_details;
                order.order_items = cart.cart_items;
                order.sub_total = cart.sub_total.toFixed(2);
                order.discount = cart.discount.toFixed(2);
                order.tax = cart.tax.toFixed(2);
                order.total_paid_amount = cart.total_paid_amount.toFixed(2);
                order.payment_card_id = maskify(booking_data.payment_card_id);
                order.payment_status = true ;
                await order.save();

                if (order){
                    await Cart.findOneAndUpdate({
                            cart_unique_id: booking_data.cart_unique_id,
                        },
                        {
                            $set: {
                                cart_items : [],
                                sub_total: 0,
                                tax: 0,
                                discount: 0,
                                total_paid_amount: 0,
                            }
                        }, {returnOriginal: false});

                    await BillingAddress.findOneAndUpdate({
                        user_id: user.id
                    }, {
                        $set: {
                            billing_address : booking_data.booking_details
                        },
                    }, {returnOriginal: false});

                    //Add Update Cart With Package
                    await updateOrCreate({
                        user_id: user ? user._id : null,
                        billing_address:  booking_data.booking_details
                    }, user ? user._id : null);
                }
                return  order ;

            } else {
                return new ApolloError("Please add product on cart to proceed");
            }
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error);
        }
    },
};

function updateOrCreate(data, user_id) {
    return BillingAddress.findOne({user_id: user_id})
        .then(function (obj) {
            // update
            if (obj) {
                return obj.updateOne(data);
            }
            return BillingAddress.create(data);
        })
}

export default bookingMutations;
