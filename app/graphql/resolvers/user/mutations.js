import {Cart, Otp, User} from './../../../Models'
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import * as yup from "yup";
import {ApolloError} from 'apollo-server-errors';
import {notification, smsSender, storeFirebaseToken, uploadFiles} from "../../../Utils/Helpers";
import {S3} from "aws-sdk";
const mongoose = require('mongoose');
require('dotenv').config();


const createUserSchema = yup.object().shape({
    first_name: yup.string().min(3, 'first_name must be more than 3 characters').max(100).required({first_name: 'first_name is required'}),
    last_name: yup.string().min(2, 'last_name must be more than 2 characters').max(100).required({last_name: 'last_name is required'}),
    email: yup.string().min(3, 'email must be more than 3 characters').max(100).email().required({email: 'email is required'}),
    phone: yup.string().min(3).max(100),
    password: yup.string().min(8, 'password must be more than 8 characters').max(100).required({password: 'password is required'}),
    confirm_password: yup.string().required()
        .oneOf([yup.ref('password'), null], 'passwords must match')

})

const forgotSchema = yup.object().shape({
    phone: yup.string().min(3).max(100),
    password: yup.string().min(8, 'password must be more than 8 characters').max(100).required({password: 'password is required'}),
    confirm_password: yup.string().required()
        .oneOf([yup.ref('password'), null], 'passwords must match')
})

const resetSchema = yup.object().shape({
    current_password: yup.string().min(8, 'current_password must be more than 8 characters').max(100).required({current_password: 'current_password is required'}),
    new_password: yup.string().min(8, 'new_password must be more than 8 characters').max(100).required({new_password: 'new_password is required'}),
    confirm_password: yup.string().required().oneOf([yup.ref('new_password'), null], 'new_password and confirm password must match')
})


const socialSchema = yup.object().shape({
    social_login_type: yup.number().required({social_login_type: 'social_login_type is required'}),
    social_login_id: yup.string().required({social_login_id: 'social_login_id is required'}),
});

const updateUserSchema = yup.object().shape({
    first_name: yup.string().min(3, 'first_name must be more than 3 characters').max(100).required({first_name: 'first_name is required'}),
    last_name: yup.string().min(3, 'last_name must be more than 3 characters').max(100).required({last_name: 'last_name is required'}),
    email: yup.string().min(3, 'email must be more than 3 characters').max(100).email().required({email: 'email is required'}),
    phone: yup.string().min(3).max(100),
    dob: yup.string().optional(),
    marital_status: yup.number().min(0).max(2).required({marital_status: "marital_status is required"}),
    gender: yup.number().min(0).max(2).required({gender: "gender is required"}),
})

const createBusinessSchema = yup.object().shape({
    first_name: yup.string().min(3, 'first_name must be more than 3 characters').max(100).required({first_name: 'first_name is required'}),
    last_name: yup.string().min(3, 'last_name must be more than 3 characters').max(100).required({last_name: 'last_name is required'}),
    email: yup.string().min(3, 'email must be more than 3 characters').max(100).email().required({email: 'email is required'}),
    phone: yup.string().min(3).max(100),
    password: yup.string().min(8, 'password must be more than 8 characters').max(100).required({password: 'password is required'}),
    confirm_password: yup.string().required()
        .oneOf([yup.ref('password'), null], 'passwords must match'),
    business: yup.object().shape({
        business_name: yup.string().required('business_name is required'),
        business_category: yup.array().required("business_category is required"),
        business_region: yup.string().required("business_region is required"),
        business_postcode: yup.string().required("business_postcode is required"),
        business_address: yup.string().required("business_address is required"),
        business_trip_planner: yup.boolean().required({business_trip_planner: "business_trip_planner is required"}),
    }),
})

const accountSettingSchema = yup.object().shape({
    email: yup.object().shape({
        offer_and_discount: yup.boolean().required('offer_and_discount is required'),
        booking_updates: yup.boolean().required("booking_updates is required"),
        event_activities: yup.boolean().required("event_activities is required"),
    }),
})

const updateBusinessSchema = yup.object().shape({
    first_name: yup.string().min(3, 'first_name must be more than 3 characters').max(100).required({first_name: 'first_name is required'}),
    last_name: yup.string().min(3, 'last_name must be more than 3 characters').max(100).required({last_name: 'last_name is required'}),
    email: yup.string().min(3, 'email must be more than 3 characters').max(100).email().required({email: 'email is required'}),
    phone: yup.string().min(3).max(100),
    business: yup.object().shape({
        business_name: yup.string().required('business_name is required'),
        business_category: yup.array().required("business_category is required"),
        business_region: yup.string().required("business_region is required"),
        business_postcode: yup.string().required("business_postcode is required"),
        business_address: yup.string().required("business_address is required"),
    }),
})

const loginSchema = yup.object().shape({
    email: yup.string().min(3, 'email must be more than 3 characters').max(100).email().required({email: 'email is required'}),
    password: yup.string().min(3, 'password must be more than 3 characters').max(100).required({password: 'password is required'}),
})

const userMutations = {
    createUser: async (_, {user}) => {
        try {
            await createUserSchema.validate(user, {abortEarly: false});

            user.password = await bcrypt.hash(user.password, await bcrypt.genSalt(10));
            const existing = await User.find({email: user.email.toLowerCase() });
            if (existing.length > 0) {
                throw new ApolloError('Email already exists');
            }
            const existingPhone = await User.find({phone: user.phone});
            if (existingPhone.length > 0) {
                throw new ApolloError('Phone already exists');
            }
            user.phone_verified_at = "";
            user.email_verified_at = "";
            user.gender = 0;
            user.dob = "";
            user.marital_status = 0;
            user.google_id = "";
            user.facebook_id = "";
            user.apple_id = "";
            user.role = "user";
            user.profile_image = "";
            user.email = user.email.toLowerCase();
            user.notifications = {
                  email : {
                      offer_and_discount : false,
                      booking_updates : false,
                      event_activities : false
                  },
                 text : {
                     offer_and_discount : false,
                     booking_updates : false,
                     event_activities : false
                 }
            };
            user.firebase_tokens = [];
            const newUser = new User(user);
            const token = jwt.sign({
                id: newUser.id,
                email: newUser.email,
                issued: Date.now()
            }, process.env.JWT_SECRET);
            Object.assign(newUser, {token});

            try {
                var otp = '';
                let randomNumber = Array(6).fill("0123456789").map(function (x) {
                    return x[Math.floor(Math.random() * x.length)]
                }).join('');
                otp = Number(randomNumber);
                var phone = user.phone;
                const newOtp = new Otp({phone, otp});
                smsSender(phone, otp.toString());
                await newOtp.save();
            } catch (err) {
                var error1 = err.inner && err.inner.length ? err.inner[0].message : err;
                return new ApolloError(error1)
            }
            var createdUser = await newUser.save();
            if (user.cart_unique_id !== null && user.cart_unique_id !== "") {
                var cart_unique_id = await Cart.findOne({'cart_unique_id': user.cart_unique_id});
                if (cart_unique_id) {
                    await Cart.findOneAndUpdate({cart_unique_id: user.cart_unique_id}, {
                        $set: {
                            user_id: createdUser._id
                        },
                    }, {returnOriginal: false});
                }
            }

            if (user.firebase !== undefined && user.firebase.token !== null && user.firebase.token !== ""){
                await storeFirebaseToken(createdUser,user.firebase)
            }

            return createdUser;

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },


    updateUser: async (_, {user}, context) => {
        try {
            await updateUserSchema.validate(user, {abortEarly: false});
            let user_info = context.user;
            const existing = await User.find({email: user.email});
            if (user_info.email !== user.email && existing.length > 0) {
                throw new ApolloError('Email already exists');
            }
            const existingPhone = await User.find({phone: user.phone});
            if (user_info.phone !== user.phone && existingPhone.length > 0) {
                throw new ApolloError('Phone already exists');
            }
            return await User.findOneAndUpdate({_id: user_info._id}, {
                $set: {
                    first_name: user.first_name,
                    last_name: user.last_name,
                    dob: user.dob,
                    email: user.email,
                    phone: user.phone,
                    email_verified_at : user.email !== user_info.email ? "" : user_info.email_verified_at,
                    phone_verified_at : user.phone !== user_info.phone ? "" : user_info.phone_verified_at,
                    marital_status: user.marital_status,
                    gender: user.gender
                },
            }, {returnOriginal: false});

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },


    updateBusiness: async (_, {user}, context) => {
        try {
            await updateBusinessSchema.validate(user, {abortEarly: false});
            let user_info = context.user;
            const existing = await User.find({email: user.email});
            if (user_info.email !== user.email && existing.length > 0) {
                throw new ApolloError('Email already exists');
            }
            const existingPhone = await User.find({phone: user.phone});
            if (user_info.phone !== user.phone && existingPhone.length > 0) {
                throw new ApolloError('Phone already exists');
            }
            return await User.findOneAndUpdate({_id: user_info._id}, {
                $set: {
                    first_name: user.first_name,
                    last_name: user.last_name,
                    email_verified_at : user.email !== user_info.email ? "" : user_info.email_verified_at,
                    phone_verified_at : user.phone !== user_info.phone ? "" : user_info.phone_verified_at,
                    email: user.email,
                    phone: user.phone,
                    business: user.business
                },
            }, {returnOriginal: false});

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },


    createBusiness: async (_, {user}) => {
        try {

            await createBusinessSchema.validate(user, {abortEarly: false});

            user.password = await bcrypt.hash(user.password, await bcrypt.genSalt(10));
            const existing = await User.find({email: user.email.toLowerCase()});
            if (existing.length > 0) {
                throw new ApolloError('Email already exists');
            }
            const existingPhone = await User.find({phone: user.phone});
            if (existingPhone.length > 0) {
                throw new ApolloError('Phone already exists');
            }
            user.phone_verified_at = "";
            user.email_verified_at = "";
            user.profile_image = "";
            user.role = "provider";
            user.email = user.email.toLowerCase();
            user.notifications = {
                email : {
                    offer_and_discount : true,
                    booking_updates : true,
                    event_activities : true
                },
                text : {
                    offer_and_discount : true,
                    booking_updates : true,
                    event_activities : true
                }
            },
            user.firebase_tokens = [];
            const newUser = new User(user);
            const token = jwt.sign({
                id: newUser.id,
                email: newUser.email,
                issued: Date.now()
            }, process.env.JWT_SECRET);
            Object.assign(newUser, {token});

            try {
                var otp = '';
                let randomNumber = Array(6).fill("0123456789").map(function (x) {
                    return x[Math.floor(Math.random() * x.length)]
                }).join('');
                otp = Number(randomNumber)
                var phone = user.phone;
                const newOtp = new Otp({phone, otp});
                // smsSender(phone, otp.toString());
                newOtp.save();
            } catch (error) {
                console.log(error)
                return new ApolloError(error.inner[0].message)
            }
            var createdProvider = newUser.save();

            if (user.firebase !== undefined && user.firebase.token !== null && user.firebase.token !== ""){
                storeFirebaseToken(createdProvider,user.firebase)
             }

            return  createdProvider ;

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error);
        }
    },


    login: async (_, {user}) => {
        try {
            await loginSchema.validate(user, {abortEarly: false});
            const newUser = await User.find({email: user.email.toLowerCase()});
            if (newUser.length > 0) {
                let validPassword = await bcrypt.compare(user.password, newUser[0].password);
                if (validPassword) {
                    const token = jwt.sign({
                        id: newUser[0].id,
                        email: newUser[0].email,
                        issued: Date.now()
                    }, process.env.JWT_SECRET);
                    Object.assign(newUser[0], {token});

                    if (user.cart_unique_id !== null && user.cart_unique_id !== "") {
                        var user_cart_unique_id = await Cart.findOne({'cart_unique_id': user.cart_unique_id});
                        if (user_cart_unique_id) {
                            await Cart.findOneAndUpdate({cart_unique_id: user.cart_unique_id}, {
                                $set: {
                                    user_id: newUser[0]._id
                                },
                            }, {returnOriginal: false});
                        }
                    }

                    if (user.firebase !== undefined && user.firebase.token !== null && user.firebase.token !== ""){
                       await storeFirebaseToken(newUser[0],user.firebase)
                    }

                    return newUser[0];

                } else {
                    throw new ApolloError('Wrong Password');
                }
            } else {
                throw new ApolloError('User does not exists');
            }

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error);
        }
    },

    forgotPassword: async (_, {data}) => {
        try {
            await forgotSchema.validate(data, {abortEarly: false});
            const newUser = await User.find({phone: data.phone});
            if (newUser.length > 0) {
                try {
                    const existing = await Otp.find({phone: data.phone});
                    if (existing.length > 0) {
                        if (existing[0].otp === data.otp) {
                            const destroyed = await Otp.deleteOne({_id: existing[0]._id});
                            if (destroyed) {
                                await User.findOneAndUpdate({phone: data.phone}, {
                                    $set: {
                                        phone_verified_at: new Date().valueOf()
                                    },
                                }, {returnOriginal: false});

                                var crypt_password = await bcrypt.hash(data.password, await bcrypt.genSalt(10));
                                return await User.findOneAndUpdate({phone: data.phone}, {
                                    $set: {
                                        password: crypt_password
                                    },
                                }, {returnOriginal: false});

                            } else {
                                return new ApolloError('Something goes wrong');
                            }
                        } else {
                            return new ApolloError('Wrong OTP')
                        }
                    } else {
                        return new ApolloError('Phone does not exists')
                    }

                } catch (err) {
                    var error1 = err.inner && err.inner.length ? err.inner[0].message : err;
                    return new ApolloError(error1);
                }
            } else {
                throw new ApolloError('User does not exists');
            }
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error);
        }
    },


    // reset the password
    resetPassword: async (_, {data},context) => {
        try {
            await resetSchema.validate(data, {abortEarly: false});
            let user = context.user ;
            let validPassword = await bcrypt.compare(data.current_password, user.password);
            if (validPassword) {
                let newPassword = await bcrypt.hash(data.new_password, await bcrypt.genSalt(10));
                const updated = await User.findOneAndUpdate({id: new mongoose.Types.ObjectId(user._id)}, {
                    $set: {
                        password: newPassword
                    },
                }, {returnOriginal: false});

                return  updated ;
            }else{
                return new ApolloError('Your current password credentials are not matched');
            }
        } catch (error) {
            return new ApolloError(error)
        }
    },

    //Login with fb/google/apple
    socialLogins: async (_, data) => {

        try {
            var social_data = data.user;
            var cart_unique_id = data.user ? data.user.cart_unique_id : "";
            await socialSchema.validate(social_data, {abortEarly: false});
            var newUser = null;
            const social_type = social_data?.social_login_type;
            switch (social_type) {

                //GOOGLE
                case 1:
                    newUser = await User.findOne({'google_id': social_data.social_login_id});
                    social_data['google_id'] = social_data.social_login_id;
                    social_data['facebook_id'] = "";
                    social_data['apple_id'] = "";
                    break;

                //Facebook
                case 2:
                    newUser = await User.findOne({'facebook_id': social_data.social_login_id});
                    social_data['facebook_id'] = social_data.social_login_id;
                    social_data['google_id'] = "";
                    social_data['apple_id'] = "";
                    break;

                //Apple
                case 3:
                    newUser = await User.findOne({'apple_id': social_data.social_login_id});
                    social_data['apple_id'] = social_data.social_login_id;
                    social_data['facebook_id'] = "";
                    social_data['google_id'] = "";
                    break;

                default:
                    return new ApolloError("something goes wrong with social login type");
            }

            if (!newUser) {
                social_data ? delete social_data.cart_unique_id : "";
                social_data ? delete social_data.social_login_id : "";
                social_data ? delete social_data.social_login_type : "";
                social_data.gender = 0;
                social_data.dob = "";
                social_data.marital_status = 0;
                social_data.phone_verified_at = "";
                social_data.email_verified_at = Date.now();
                social_data.role = "user";
                social_data.password = await bcrypt.hash(Date.now().valueOf().toString(), await bcrypt.genSalt(10));
                newUser = new User(social_data);
                newUser.save();
            }

            if (cart_unique_id !== null && cart_unique_id !== "") {
                var user_cart_unique_id = await Cart.findOne({'cart_unique_id': cart_unique_id});
                if (user_cart_unique_id) {
                    await Cart.findOneAndUpdate({cart_unique_id: cart_unique_id}, {
                        $set: {
                            user_id: newUser._id
                        },
                    }, {returnOriginal: false});
                }
            }

            const token = jwt.sign({
                id: newUser.id,
                email: newUser.email,
                issued: Date.now()
            }, process.env.JWT_SECRET);
            Object.assign(newUser, {token});

            return newUser;

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error);
        }
    },


    userAccountSettings: async (_, data, context) => {
        try {
            await accountSettingSchema.validate(data.notification, {abortEarly: false});
            let user_info = context.user;
            return await User.findOneAndUpdate({_id: user_info._id}, {
                $set: {
                    notifications: data.notification,
                },
            }, {returnOriginal: false});
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    userProfileImage: async (_, {image}, context)=>{
        try {
            let user = context.user;
             return  await User.findOneAndUpdate({_id: user._id}, {
                $set: {
                    profile_image: image
                },
            }, {returnOriginal: false});
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error);
        }
    },
};


export default userMutations;
