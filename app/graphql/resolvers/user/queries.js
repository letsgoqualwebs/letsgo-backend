import {Experience, Reviews, User, UserNotification} from "../../../Models";
import mongoose from "mongoose";
import experience from "../../../Models/User/experience";
import {experienceReviewRatings} from "../../../Utils/Helpers";

const userQueries = {

    users: async (_, args, context) => {
        const users = await User.find();
        return users;
    },

    getUserNotifications: async (_, {data}, context) => {
        const user = context.user;
        return await UserNotification.find({'user_id': new mongoose.Types.ObjectId(user._id)})
    },

    business: async (_, businessId) => {
        const experiences = await Experience.find({business_id: new mongoose.Types.ObjectId(businessId)});
        const experience_ids = experiences.length > 0 ? experiences.map((value)=>{
            value._id
        }) : [] ;

        var ratings = await Reviews.find({$in: { experience_id : experience_ids } });
        var experience_rating = [] ;
        var count = 0 ;
        ratings.length > 0 ? ratings.map((item) => {
            var total_ratings = [];
            item.review ? count++ : "";
            item.ratings ? item.ratings.map((rate) => {
                total_ratings.push(rate.rating)
            }) : [];
            item.overall_rate = total_ratings.length > 0 ? (total_ratings.reduce((a, b) => a + b, 0) / total_ratings.length).toFixed(1) : 0;
            total_ratings.length > 0 ? experience_rating.push(total_ratings.reduce((a, b) => a + b, 0) / total_ratings.length) : 0;
        }) : [];

        var business =  await User.findOne({_id: new mongoose.Types.ObjectId(businessId)});
         business['overall_rate'] = experience_rating.length > 0 ? (experience_rating.reduce((a, b) => a + b, 0) / experience_rating.length).toFixed(1) : 0;
         business['reviews'] = ratings.length;
        return business ;
    },

    listOfExperts: async (_, args) => {
        const experts = await User.find({role: "provider"}).sort({createdAt: "DESC"});
        experts && experts.length ? experts.map((expert, key) => {
            expert.overall_rate = 0;
            expert.reviews = 0;
            expert.profile_image = expert.profile_image ? expert.profile_image : "https://upay.qualwebs.com/petecation/public/storage/letsgo/1646119043pexels-jeshootscom-4.jpeg";
        }) : [];
        return experts;
    },

    listOfIndividualExperts: async (_, args) => {
        const experts = await User.find({role: "provider"}).sort({createdAt: "ASC"});
        experts && experts.length ? experts.map((expert, key) => {
            expert.overall_rate = 0;
            expert.reviews = 0;
            expert.expertise = "photographer";
            expert.profile_image = expert.profile_image ? expert.profile_image : "https://upay.qualwebs.com/petecation/public/storage/letsgo/1646119043pexels-jeshootscom-4.jpeg";
        }) : [];
        return experts;
    },

    user: async (_, {data}, context) => {
        return context.user;
    },


    getAllProviders: async (_, params) => {

        const ProviderData = await User.find({role: "provider"});

        var response = [];

        for (var j = 0; j < ProviderData.length; j++) {

            //provider info
            var provider_id = ProviderData[j].id;
            var ProviderCategory = ProviderData[j].business.business_category;
            const expData = await Experience.find({business_id: new mongoose.Types.ObjectId(provider_id)});


            for (var i = 0; i < expData.length; i++) {

                var count = 0;
                var experience_id = expData[i].id;
                var experience_rating = [];

                //Experience ratings
                var ratings = await Reviews.find({experience_id: new mongoose.Types.ObjectId(experience_id)});
                ratings.map((item, key) => {
                    var total_ratings = [];
                    item.review ? count++ : "";
                    item.ratings ? item.ratings.map((rate, key) => {
                        total_ratings.push(rate.rating)
                    }) : [];
                    item.overall_rate = total_ratings.length > 0 ? (total_ratings.reduce((a, b) => a + b, 0) / total_ratings.length).toFixed(1) : 0;
                    total_ratings.length > 0 ? experience_rating.push(total_ratings.reduce((a, b) => a + b, 0) / total_ratings.length) : 0;
                });

                var final_rating = experience_rating.length > 0 ? (experience_rating.reduce((a, b) => a + b, 0) / experience_rating.length).toFixed(1) : 0;

                //Experience price
                var total_price = [];
                expData[i].experience_package.map((item, key) => {
                    item.package_group ? item.package_group.map((prices, key) => {
                        total_price.push(prices.price)
                    }) : []
                });
                var min_price = total_price.length > 0 ? (total_price.reduce((a, b) => a < b ? a : b)) : 0;
            }


            const value = {
                id: provider_id,
                overall_rate: final_rating,
                city: ProviderData[j]?.business?.business_region,
                review: count,
                title: ProviderData[j]?.business?.business_name,
                category: ProviderCategory,
                image: ProviderData[j]?.profile_image,
                price: min_price
            };
            response.push(value);
        }
        return response;
    },

    getSingleProvider: async (_, businessId) => {

        var profile_data = await User.findOne({_id: new mongoose.Types.ObjectId(businessId)});
        var filters = {
            business_id : new mongoose.Types.ObjectId(businessId),
            is_published: 1,
            status: true
        };
        var count = 0 ;
        var final_rating = 0 ;
        var gallery = [];
        var experience_rating = [] ;
        const experiences = await experience.find(filters);

        if (experiences.length > 0){
            for (var i = 0; i < experiences.length; i++) {

                gallery.push({title: experiences[i]?.experience_details?.title, images: experiences[i]?.experience_gallery});

                var ratings = await Reviews.find({experience_id: new mongoose.Types.ObjectId(experiences[i]._id)});
                ratings.map((item, key) => {
                    var total_ratings = [];
                    item.review ? count++ : "";
                    item.ratings ? item.ratings.map((rate, key) => {
                        total_ratings.push(rate.rating)
                    }) : [];
                    item.overall_rate = total_ratings.length > 0 ? (total_ratings.reduce((a, b) => a + b, 0) / total_ratings.length).toFixed(1) : 0;
                    total_ratings.length > 0 ? experience_rating.push(total_ratings.reduce((a, b) => a + b, 0) / total_ratings.length) : 0;
                });
                 final_rating = experience_rating.length > 0 ? (experience_rating.reduce((a, b) => a + b, 0) / experience_rating.length).toFixed(1) : 0;
            }
        }

        const myExperiences = experienceReviewRatings(experiences);

        return {
            provider_profile : profile_data,
            title: profile_data?.business?.business_name,
            about : profile_data?.business?.business_overview ?  profile_data?.business?.business_overview : "",
            overall_rate: final_rating,
            review: count,
            events_assisted: 0,
            in_providers: 0,
            in_category: 0,
            exp_gallery: gallery,
            my_experience: myExperiences,
        };
    },
};

export default userQueries;
