import {ApolloError} from 'apollo-server-errors';
import {Cart} from "../../../Models";
import moment from "moment";
import * as yup from "yup";
import experience from "../../../Models/User/experience";
import userData from "../../../Models/Auth/user";

const addToCartSchema = yup.object().shape({
    cart_unique_id: yup.string().optional(),
    package_experience_id: yup.string().required({package_experience_id: 'package_experience_id is required'}),
    package_experience_title: yup.string().required({package_experience_title: 'package_experience_title is required'}),
    package_id: yup.number().required({package_id: 'package_id is required'}),
    package_title: yup.string().required({package_title: 'package_title is required'}),
    package_image: yup.string().required({package_image: 'package_image is required'}),
    start_date: yup.date().required({start_date: 'start_date is required'}),
    end_date: yup.date().required({end_date: 'end_date is required'}),
    start_time: yup.string().required({start_time: 'start_time is required'}),
    end_time: yup.string().required({end_time: 'end_time is required'}),
    package_group: yup.array().of(
        yup.object().shape({
            price: yup.number().required('price is required'),
            group: yup.string().required("group is required"),
            number_of_tickets: yup.string().required("number_of_tickets is required"),
            people_per_group: yup.number().required("people_per_group is required"),
        }))
});


const removeFromCartSchema = yup.object().shape({
    cart_unique_id: yup.string().required({cart_unique_id: 'cart_unique_id is required'}),
    package_experience_id: yup.string().required({package_experience_id: 'package_experience_id is required'}),
    package_id: yup.number().required({package_id: 'package_id is required'}),
});

const cartMutations = {

    addToCart: async (_, cart, context) => {

        await addToCartSchema.validate(cart.cart, {abortEarly: false});

        try {

            //Manage user with cart availability
           const cart_data = cart ? cart.cart : null ;
           let user = context.user;
            var cart_unique_id = cart_data.cart_unique_id;
            var userCart = null;
            if (user) {
                userCart = await Cart.findOne({user_id: user._id});
                cart_unique_id = userCart ? userCart.cart_unique_id : null;
                if (!cart_unique_id) {
                    cart_unique_id = Math.floor(Math.random() * 26) + moment().valueOf();
                }
            } else {
              if (!cart_unique_id) {
                  cart_unique_id = Math.floor(Math.random() * 26) + moment().valueOf();
                }
            }


            //Add Update Cart With Package
            await updateOrCreate({
                user_id: user ? user._id : null,
                cart_unique_id: cart_unique_id
            }, cart_unique_id);

            userCart = await Cart.findOne({cart_unique_id: cart_unique_id});

            const cartItems = userCart.cart_items;
            var experience_package = false;
            var index = null ;

            cartItems.map((item,key) => {
              if (item.package_experience_id === cart_data.package_experience_id && item.package_id === cart_data.package_id){
                  experience_package = true ;
                  index = key ;
                }
            });

            var experience_info = await experience.findOne({'_id':cart_data.package_experience_id});
            var user_info = await userData.findOne({'_id':experience_info.business_id});
            var final_cart_items = [] ;
            if (experience_package === false ) {

               await cartItems.push({
                  business_id : experience_info.business_id,
                  business_category : experience_info ? experience_info.experience_details.category : "",
                  business_provider : user_info ? user_info.first_name +" "+ user_info.last_name : "",
                  business_location : experience_info ? experience_info.experience_details.city : "",
                  business_days : experience_info ? experience_info.experience_details.days : 0,
                  start_date: cart_data.start_date,
                  end_date: cart_data.end_date,
                  start_time: cart_data.start_time,
                  end_time: cart_data.end_time,
                  package_experience_id: cart_data.package_experience_id,
                  package_experience_title: cart_data.package_experience_title,
                  package_id: cart_data.package_id,
                  package_title: cart_data.package_title,
                  package_image: cart_data.package_image,
                  package_groups: cart_data.package_groups,
                });
                userCart.cart_items = cartItems ;
                userCart.save();
                final_cart_items = cartItems ;

            }else{

                if (index !== null) {
                    await Cart.findOneAndUpdate({
                           cart_unique_id: cart_unique_id,
                        },
                      {
                          $set: {[`cart_items.${index}`] : {
                                  business_id : experience_info.business_id,
                                  business_category : experience_info ? experience_info.experience_details.category : "",
                                  business_provider : user_info ? user_info.first_name +" "+ user_info.last_name : "",
                                  business_location : experience_info ? experience_info.experience_details.city : "",
                                  business_days : experience_info ? experience_info.experience_details.days : 0,
                                  start_date: cart_data.start_date,
                                  end_date: cart_data.end_date,
                                  start_time: cart_data.start_time,
                                  end_time: cart_data.end_time,
                                  package_experience_id: cart_data.package_experience_id,
                                  package_experience_title: cart_data.package_experience_title,
                                  package_id: cart_data.package_id,
                                  package_title: cart_data.package_title,
                                  package_image: cart_data.package_image,
                                  package_groups: cart_data.package_groups,
                          }}
                      }, {returnOriginal: false});

                     var final_cart = await Cart.findOne({cart_unique_id: cart_unique_id}) ;
                     final_cart_items = final_cart ? final_cart.cart_items : [];
                }
            }


            //Price Calculations
            const price_array = [] ;
            final_cart_items.length ?
                final_cart_items.map(item => {
                    item.package_groups.map(group => {
                        price_array.push(group['price'] * group['number_of_tickets']);
                    })
                }) : "" ;
            const sub_total_price =  price_array.reduce((a, b) => a + b, 0);
            const tax  = sub_total_price * 5 /100 ;
            const discount  = 0 ;
            const total_paid_amount = tax + sub_total_price - discount ;

            await Cart.findOneAndUpdate({
                    cart_unique_id: cart_unique_id,
                },
                {
                    $set: {
                        sub_total: sub_total_price.toFixed(2),
                        tax: tax.toFixed(2),
                        discount: discount.toFixed(2),
                        total_paid_amount: total_paid_amount.toFixed(2),
                    }
                }, {returnOriginal: false});

            return {cart_unique_id: cart_unique_id};

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error);
        }
    },

    removeFromCart: async (_, cart, context) => {

        await removeFromCartSchema.validate(cart.cart, {abortEarly: false});
        try {

            const cart_data = cart.cart ;
            const cart_unique_id = cart_data.cart_unique_id ;
            const userCart = await Cart.findOne({cart_unique_id: cart_unique_id});
            const cart_items = userCart ? userCart.cart_items : [];
            var final_cart_items = [] ;
            cart_items.length ?
                cart_items.map((item,key) => {
                    if (!(item.package_experience_id === cart_data.package_experience_id && item.package_id === cart_data.package_id)){
                        final_cart_items.push(item)
                    }
                })
                : "" ;
            userCart.cart_items = final_cart_items;
            userCart.save();


            //Price Calculations
            var price_array = [] ;
            final_cart_items.length ?
                final_cart_items.map(item => {
                    item.package_groups.map(group => {
                        price_array.push(group['price']*group['number_of_tickets']);
                    })
                }) : "" ;
            const sub_total_price =  price_array.reduce((a, b) => a + b, 0);
            const tax  = sub_total_price * 5 /100 ;
            const discount  = 0 ;
            const total_paid_amount = tax + sub_total_price - discount ;

            await Cart.findOneAndUpdate({
                    cart_unique_id: cart_unique_id,
                },
                {
                    $set: {
                        sub_total: sub_total_price.toFixed(2),
                        tax: tax.toFixed(2),
                        discount: discount.toFixed(2),
                        total_paid_amount: total_paid_amount.toFixed(2),
                    }
                }, {returnOriginal: false});

            return {cart_unique_id: cart_unique_id};

        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error);
        }
    },
};

function updateOrCreate(values, cart_unique_id) {
    return Cart.findOne({cart_unique_id: cart_unique_id})
        .then(function (obj) {
            // update
            if (obj) {
                return obj.updateOne(values);
            }
            return Cart.create(values);
    })
}


export default cartMutations;
