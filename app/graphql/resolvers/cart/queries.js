import {Cart} from "../../../Models";
import {ApolloError} from "apollo-server-errors";

const cartQueries = {

    cart_items: async (_, data,context) => {
        try {

            let user = context.user ;
            var UserCart = await Cart.findOne({'cart_unique_id': data.id});
            if (user){
                 UserCart = await Cart.findOne({'user_id': user._id});
            }
            const cart_items = UserCart ? UserCart.cart_items : [];
            const package_items = [];
            cart_items.map((item, key) => {
                var experience_id = item.package_experience_id;
                var package_id = item.package_id;
                var experience_title = item.package_experience_title;
                var package_title = item.package_title ;
                var package_image = item.package_image ;
                var group = [] ;
                var price = [] ;
                item.package_groups.map((group_details, key) => {
                    group.push(group_details['group']);
                    price.push(group_details['price'] * group_details['number_of_tickets']);
                });
               var group_string = group.join(' | ');
               var total_price = price.reduce((a, b) => a + b, 0);

                package_items.push({
                    'package_experience_id' : experience_id,
                    'package_id' : package_id,
                    'ExperienceTitle' : experience_title,
                    'PackageTitle' : package_title,
                    'PackageImage' : package_image,
                    'Group' : group_string,
                    'Price' : total_price
                })

            });
            UserCart['package_items'] = package_items ;
            return UserCart
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },

    singlePackageItems: async (_, data) => {
        try {
            const UserCart = await Cart.findOne({'cart_unique_id': data.id});
            const cart_items = UserCart ? UserCart.cart_items : [];
            const final_cart_items = [];
            cart_items.map((item, key) => {
                if (item.package_experience_id === data.package_experience_id && item.package_id === data.package_id) {
                    final_cart_items.push(item);
                }
            });
            return final_cart_items
        } catch (err) {
            var error = err.inner && err.inner.length ? err.inner[0].message : err;
            return new ApolloError(error)
        }
    },
};

export default cartQueries;
