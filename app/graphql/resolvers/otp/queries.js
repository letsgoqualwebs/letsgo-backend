import {Otp} from "../../../Models";


const otpQueries = {
  otp : async (_, args) => {
    return await Otp.find();
  },
}

export default otpQueries;