import {EmailOtp, Otp, User} from './../../../Models'
import {ApolloError} from 'apollo-server-errors';
import {emailOtpSender, smsSender} from '../../../Utils/Helpers';
import jwt from "jsonwebtoken";

const otpMutations = {
  generateOtp: async (_, {phone}) => {
    try {
      var otp = '';

      let randomNumber = Array(6).fill("0123456789").map(function (x) {
        return x[Math.floor(Math.random() * x.length)]
      }).join('');
      otp = Number(randomNumber);
      const existing = await Otp.find({phone: phone});
      if (existing.length > 0) {
        const updated = await Otp.findOneAndUpdate({_id: existing[0]._id}, {
          $set: {
            otp: otp
          },
        }, {returnOriginal: false});
        smsSender(phone, otp.toString());
        return updated;
      }
      const newOtp = new Otp({phone, otp});
      smsSender(phone, otp.toString());
      return newOtp.save();

    } catch (error) {
      console.log(error)
      return new ApolloError(error.inner[0].message)
    }
  },

  generateEmailOtp: async (_, {email}) => {
    try {
      var otp = '';
      let randomNumber = Array(6).fill("0123456789").map(function (x) {
        return x[Math.floor(Math.random() * x.length)]
      }).join('');
      otp = Number(randomNumber)
      const existing = await EmailOtp.find({email: email});
      if (existing.length > 0) {
        const updated = await EmailOtp.findOneAndUpdate({_id: existing[0]._id}, {
          $set: {
            otp: otp
          },
        }, {returnOriginal: false});
        emailOtpSender(email, otp.toString());
        return updated;
      }
      const newOtp = new EmailOtp({email, otp});
      emailOtpSender(email, otp.toString());
      return newOtp.save();

    } catch (error) {
      console.log(error)
      return new ApolloError(error.inner[0].message)
    }
  },

  verifyOtp: async (_, {phone, otp}) => {
    try {
      const existing = await Otp.find({phone: phone});
      if (existing.length > 0) {
        // if (existing[0].otp == otp) {
          const destroyed = await Otp.deleteOne({_id: existing[0]._id});
          // if (destroyed) {
          var user =  await User.findOneAndUpdate({phone: phone}, {
              $set: {
                phone_verified_at: new Date().valueOf()
              },
            }, {returnOriginal: false});
          // }
          const token = jwt.sign({
            id: user.id,
            email: user.email,
            issued: Date.now()
          }, process.env.JWT_SECRET);
          Object.assign(user, {token});
          return user;
        // } else {
        //   return new ApolloError('Wrong OTP');
        // }
      } else {
        return new ApolloError('Phone does not exists')
      }
    } catch (error) {
      return new ApolloError(error.inner[0].message)
    }
  }
  ,

  verifyEmailOtp: async (_, {email, otp}) => {
    try {
      const existing = await EmailOtp.find({email: email});
      if (existing.length > 0) {
        if (existing[0].otp == otp) {
          const destroyed = await EmailOtp.deleteOne({_id: existing[0]._id});
          if (destroyed) {
            var user =  await User.findOneAndUpdate({email: email}, {
              $set: {
                email_verified_at: new Date().valueOf()
              },
            }, {returnOriginal: false});
          }
          return user;
        } else {
          return new ApolloError('Wrong OTP');
        }
      } else {
        return new ApolloError('Email does not exists')
      }
    } catch (error) {
      return new ApolloError(error.inner[0].message)
    }
  }
};

export default otpMutations;
