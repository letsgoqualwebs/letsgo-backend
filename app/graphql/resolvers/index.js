import { userQueries, userMutations } from "./user";
import {categoryQueries} from "./common/category";
import {otpMutations, otpQueries} from "./otp";
import {experienceMutations, experienceQueries} from "./experience";
import {uploadMutation} from "./fileUpload";
import {cartMutation, cartQueries} from "./cart";
import {bookingMutations, bookingQueries} from "./booking";
import reviewRatingMutations from "./rating_and_reviews/mutations";
import reviewQueries from "./rating_and_reviews/queries";
import {eventMutations, eventQueries} from "./events";
import {chatMutations, chatQueries} from "./chat";
const resolvers = {
    Query: {
        ...userQueries,
        ...categoryQueries,
        ...otpQueries,
        ...experienceQueries,
        ...cartQueries,
        ...bookingQueries,
        ...reviewQueries,
        ...eventQueries,
        ...chatQueries
    },
    Mutation: {
        ...userMutations,
        ...otpMutations,
        ...experienceMutations,
        ...uploadMutation,
        ...cartMutation,
        ...bookingMutations,
        ...reviewRatingMutations,
        ...eventMutations,
        ...chatMutations,
        ...uploadMutation
    }
};

export default resolvers;
