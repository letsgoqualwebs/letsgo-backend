import {EventReview} from "../Models";
require('dotenv').config();
const aws = require('aws-sdk');
const Busboy = require('busboy');
const axios = require('axios');
const FormData = require('form-data');
const {Reviews, ChatThread, ChatLine, User, Cart, Events, BillingAddress, UserNotification} = require("../Models");
const admin = require("firebase-admin");
const serviceAccount = require("../../config/firebase.json");
const mongoose = require("mongoose");
const {getMessaging} = require("firebase-admin/messaging");
const moment = require("moment");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://let-sgo-d9cfd-default-rtdb.firebaseio.com/"
});
var db = admin.database();

const omitKey = (key, obj) => {
    const {[key]: omitted, ...rest} = obj;
    return rest;
};


const emailCreds = {
    url: 'https://esmtp.qualwebs.com/qw/api/api.php',
    key: process.env.EMAIL_API_KEY,
    client: 'Lets-Go',
    replyTo: 'noreply@letsgo.com',
    subject: 'OTP for verifying otp'
};

const s3 = new aws.S3({
    region: process.env.AWS_REGION,
    accessKeyId: process.env.AWS_ACCESS_ID,
    secretAccessKey: process.env.AWS_SECRET_KEY
});

const experienceReviewRatings = async (experiences) => {

    const temp = experiences.length > 0 ? experiences.map(async (experience, index) => {

        //Condition commented for experience un-available dates and end-date

        // var slot_end_dates = [] ;
        // experience.experience_availability && experience.experience_availability.length > 0 ? experience.experience_availability.map((availability)=>{
        //     if (availability){
        //         slot_end_dates.push(new Date(availability.available_till_date));
        //     }
        // }) : [];
        // var maxExperienceDate =  slot_end_dates.length ?
        //     new Date(
        //         Math.max(
        //             ...slot_end_dates
        //         ),
        //     ) : "" ;
        // var currentDate = moment().format('YYYY/MM/DD') ;
        // var maxExperienceNextDate = maxExperienceDate ? moment(maxExperienceDate).format('YYYY/MM/DD') : "";
        // if (maxExperienceNextDate >= currentDate) {

            var experience_rating = [];
            var ratings = await Reviews.find({experience_id: experience.id});
            ratings ? ratings.map((item, key) => {
                var total_ratings = [];
                item.ratings ? item.ratings.map((rate, key) => {
                    total_ratings.push(rate.rating)
                }) : [];
                total_ratings.length > 0 ? experience_rating.push(total_ratings.reduce((a, b) => a + b, 0) / total_ratings.length) : 0;
            }) : [];
            experience.overall_rate = experience_rating.length > 0 ? (experience_rating.reduce((a, b) => a + b, 0) / experience_rating.length).toFixed(1) : 0;
            experience.reviews = experience_rating.length > 0 ? experience_rating.length : 0;

            return  experience

        // }else{
        //     experiences.splice(index,1);
        // }

    }) : [];

    await Promise.all(temp);

    return experiences;
};


const eventReviewRatings = async (events) => {

    const temp = events ? events.map(async (event, key) => {
        var budgets = event.budget ? event.budget.split("-") : [];
        var event_rating = [];
        var ratings = await EventReview.find({event_id: event.id});
        ratings ? ratings.map((item, key) => {
            var total_ratings = [];
            item.ratings ? item.ratings.map((rate, key) => {
                total_ratings.push(rate.rating)
            }) : [];
            total_ratings.length > 0 ? event_rating.push(total_ratings.reduce((a, b) => a + b, 0) / total_ratings.length) : 0;
        }) : [];
        event.overall_rate = event_rating.length > 0 ? (event_rating.reduce((a, b) => a + b, 0) / event_rating.length).toFixed(1) : 0;
        event.filter_price = budgets.length ?  budgets[0] : 0 ;
    }) : [];

    await Promise.all(temp);

    return events;
};

const uploadFile = (request) => {
    return new Promise(async (resolve, reject) => {
        const headers = request.headers;
        const busboy = Busboy({headers});
        // you may need to add cleanup logic using 'busboy.on' events
        busboy.on('error', err => reject(err))
        busboy.on('file', function (fieldName, fileStream, fileName, encoding, mimeType) {
            const params = {
                Bucket: process.env.AWS_S3_BUCKET,
                Key: 'dev/' + fileName.filename,
                Body: fileStream,
                ACL: 'public-read'
            };
            s3.upload(params).promise().then((res) => {
                resolve(res);
            });
        });
        request.pipe(busboy)
    })
};

const maskify = (cc) => {
    return cc.replace(/.(?=.{4})/g, "#");
}

const storeFirebaseToken = async (user_details,firebase)=>{

    var firebase_tokens = user_details?.firebase_tokens ;
    var token_index = 0 ;
    firebase_tokens && firebase_tokens.length ? firebase_tokens.map((token,index)=>{
            if (token.platform === firebase?.platform){
               return token_index = index
            }
    }) : "" ;

    await User.findOneAndUpdate({
        _id: new mongoose.Types.ObjectId(user_details['_id']),
    }, {
        $set: {
            [`firebase_tokens.${token_index}`]:
                {
                    platform : firebase?.platform,
                    token : firebase?.token
            }
        }
    }, {returnOriginal: false});
};

const notification = async (user_id,type,title,message,reference)=>{

    var user = await User.findOne({_id : new mongoose.Types.ObjectId(user_id)});

    // This registration token comes from the client FCM SDKs.
    var registrationTokens = user?.firebase_tokens?.length ? user?.firebase_tokens.map((item)=>{
            return item.token ;
    }) : [];

    if (registrationTokens.length > 0) {

       const notificationCount = await UserNotification.find({user_id:user_id,read_status:false});

        //Notification DATA
         const notification_data = {

             notification: {
                 body: title.toString(),
                 title: message.toString(),
             },
             data: {
                 type: type.toString(),
                 title: title.toString(),
                 count : notificationCount ? notificationCount.length.toString() : "0",
                 message: message.toString(),
                 reference_id: reference.toString()
             },
             tokens: ["eBcNm0y6ukmTvoPfujiOu0:APA91bHltMWmXq7jJRVbZH1EaQ9KCJ3zuDqFzkTsPbCIJbsFQZ5bYsW1tyhFTvL6qK2-aBUzr5YhcyUrQ_fmAGCf0MJwh5RZC0CAgsyH5oQR3Q1fmVw-8qGOlx7y27K6o64HG5k7TWiA"]
         };

    // const notification_data = {
    //     data: {
    //         type: "test",
    //         title: "test",
    //         message: "test",
    //         reference_id: "test"
    //     },
    //     tokens: ["eCOA11ERP0aKkTPRrPBu32:APA91bG-FQr60ztWaUAcmwjAu1tW_jUa2acBZeEI3dgeO-vbg2oVereyhlGQs1CklIRyYvB4-f1AuzMjoihTfGJT-P799YnuJPYqoNYdcKVXvans3ceX_Q-HF1dI2CxO0hWVq7ATKsK9"]
    // };

        //Storage Data
        const store_data = {
            type: type.toString(),
            title: title.toString(),
            message: message.toString(),
            reference_id: reference.toString(),
            user_id : user_id,
            read_status : false
        };

        const notification = await UserNotification.create(store_data);
        // if (notification){
        //     pushFirebaseNotification(user_id,notification);
        // }

         // Send a message to the device corresponding to the provided
         // registration token.
         await getMessaging().sendMulticast(notification_data)
             .then((response) => {
                 // Response is a message ID string.
                 console.log('Successfully sent message:', response);
             })
             .catch((error) => {
                 console.log('Error sending message:', error);
             });
     }
};

// const pushFirebaseNotification = function (user_id,notification) {
//     const ref = db.ref('user_notifications');
//     ref.child(user_id).set({notification});
// };

const smsSender = (phone, otp) => {
    // var config = {
    //   method: 'get',
    //   url:`${smsCreds.url}?user=${smsCreds.username}&pass=${smsCreds.password}&to=${phone}&message=Your lets-Go one time otp is ${encodeURIComponent(otp)}&sender=${smsCreds.sender}&unicode=u`,
    //   headers: {}
    // };
    //
    // axios(config)
    //   .then(function (response) {
    //     console.log(JSON.stringify(response.data));
    //     return `${smsCreds.url}?user=${smsCreds.username}&pass=${smsCreds.password}&to=${phone}&message=${otp}&sender=${smsCreds.sender}&unicode=u`;
    //   })
    //   .catch(function (error) {
    //     console.log(error);
    //   });

    return true;

};

const emailOtpSender = (email, otp) => {
    var data = new FormData();
    data.append('email', email);
    data.append('clientid', emailCreds.client);
    data.append('key', emailCreds.key);
    data.append('subject', emailCreds.subject);
    data.append('message', `<p>Your OTP for verifing email is <strong>${otp}</strong></p>`);
    data.append('file', '');
    data.append('replyto', emailCreds.replyTo);

    var config = {
        method: 'post',
        url: emailCreds.url,
        headers: {
            ...data.getHeaders()
        },
        data: data
    };

    axios(config)
        .then(function (response) {
            console.log(JSON.stringify(response.data));
            return response.data;
        })
        .catch(async function (error) {
            console.log(error);
            return error;
        });
};

const apiResponseHandler = ($response = [], $message = '', $status = 200, $limit = '') => {
    return {'response': $response, 'message': $message, 'status': $status, 'pagination_limit ': $limit};
}

const getDaysBetweenDates = function (startDate, endDate) {
    var now = startDate.clone(), dates = [];
    while (now.isSameOrBefore(endDate)) {
        dates.push(now.format('YYYY-MM-DD'));
        now.add(1, 'days');
    }
    return dates;
};

const pushFirebaseChatThread = function (user, provider,chat_thread_id) {

    const ref = db.ref('chat_thread');

    //provider
    ref.child(user.id).child(chat_thread_id).set({
        id : chat_thread_id,
        unread_count : 0,
        last_message : null,
        last_timestamp : null,
        user : {
            id: provider.id,
            first_name: provider.first_name,
            last_name: provider.last_name,
            profile_image: provider.profile_image ? provider.profile_image : "http://54.151.160.83/petecation/public//images/profile.png"
        }
    });

    //user
    ref.child(provider.id).child(chat_thread_id).set({
        id : chat_thread_id,
        unread_count : 0,
        last_message : null,
        last_timestamp : null,
        user : {
            id: user.id,
            first_name: user.first_name,
            last_name: user.last_name,
            profile_image: user.profile_image ? user.profile_image : "http://54.151.160.83/petecation/public//images/profile.png"
        }
    })
}

const updateFirebaseChatThread = async function (chat_thread_id,sender_id,message) {

    const ref = db.ref('chat_thread');
    const thread = await ChatThread.findOne({'_id': new mongoose.Types.ObjectId(chat_thread_id)});

    //provider
    if (thread && sender_id !== thread.provider_id) {

        var provider_count = thread ? thread.provider_unread_count + 1 : 0 ;

        ref.child(thread.user_id).child(chat_thread_id).update({
            id: chat_thread_id,
            unread_count:provider_count ,
            last_message: message,
            last_timestamp: Date.now().valueOf(),
            user: {
                id: thread ? thread.provider_id : "",
                first_name: thread ? thread.provider.first_name : "",
                last_name: thread ? thread.provider.last_name : "",
                profile_image: thread && thread.provider.profile_image ? thread.provider.profile_image : "http://54.151.160.83/petecation/public//images/profile.png"
            }
        });

        await ChatThread.findOneAndUpdate({'_id': chat_thread_id}, {
            $set: {
                provider_unread_count: provider_count
            },
        }, {returnOriginal: false});
    }

    //user
    if (thread && sender_id !== thread.user_id) {

        var user_count = thread ? thread.user_unread_count + 1 : 0 ;
        ref.child(thread.provider_id).child(chat_thread_id).update({
            id: chat_thread_id,
            unread_count: user_count,
            last_message: message,
            last_timestamp: Date.now().valueOf(),
            user: {
                id: thread ? thread.user_id : "",
                first_name: thread ? thread.user.first_name : "",
                last_name: thread ? thread.user.last_name : "",
                profile_image: thread && thread.user.profile_image ? thread.user.profile_image : "http://54.151.160.83/petecation/public//images/profile.png"
            }
        })

        await ChatThread.findOneAndUpdate({'_id': chat_thread_id}, {
            $set: {
                user_unread_count: user_count
            },
        }, {returnOriginal: false});
    }
}

const pushFirebaseChatMessages = function (message,chat_thread_id,message_id) {
    const ref = db.ref('chat_line');
    ref.child(chat_thread_id).child(message_id).set({
        message
    })
}

const createOrUpdateChatThread = async function (user_id,provider_id) {

    var user = await User.findOne({'_id': new mongoose.Types.ObjectId(user_id)});
    var provider = await User.findOne({'_id': new mongoose.Types.ObjectId(provider_id)});

    var data = {
        user_id: user_id,
        provider_id: provider_id ,
        user : user ,
        provider : provider ,
        user_unread_count : 0,
        provider_unread_count : 0 ,
        last_message : null,
        last_timestamp : null
    };

    var thread = await ChatThread.findOne(
        { $or: [
                { user_id : user_id , provider_id : provider_id },
                { user_id : provider_id , provider_id : user_id }]
        }).then(function (obj) {
            // update
            if (obj) {
                return obj;
            }
            return ChatThread.create(data);
        });
    pushFirebaseChatThread(user,provider,thread.id);
    return thread ;
};

module.exports = {
    maskify,
    omitKey,
    uploadFile,
    smsSender,
    apiResponseHandler,
    getDaysBetweenDates,
    experienceReviewRatings,
    pushFirebaseChatThread,
    createOrUpdateChatThread,
    pushFirebaseChatMessages,
    updateFirebaseChatThread,
    emailOtpSender,
    storeFirebaseToken,
    notification,
    eventReviewRatings
};

