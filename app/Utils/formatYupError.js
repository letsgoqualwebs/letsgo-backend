// import { ValidationError } from "yup";

const formatYupError = (err) => {
  const errors = [];
  err.inner.forEach(e => {
    errors.push({
      path: e.path,
      message: e.message
    });
  });
  console.log(e.message);
  return errors;
};

module.exports = {
  formatYupError,
};

