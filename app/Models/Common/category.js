import {model, Schema} from 'mongoose';

const CategorySchema = new Schema(
    {
        name: {type: String, required: true},
        attributes: {type: Array, required: true},
    },
    {
        timestamps: true
    }
);

export default model('Category', CategorySchema)
