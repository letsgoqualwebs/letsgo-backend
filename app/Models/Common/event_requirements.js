import {model, Schema} from 'mongoose';

const EventRequirementSchema = new Schema(
    {
        name: {type: String, required: true},
        requirements: {type: Array, required: true},
    },
    {
        timestamps: true
    }
);

export default model('event_requirements', EventRequirementSchema)
