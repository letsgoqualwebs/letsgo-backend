import {model, Schema} from 'mongoose';

const UserNotificationSchema = new Schema(
    {
        user_id: {type: String, required: true},
        type: {type: String, required: true},
        title: {type: String, required: true},
        message: {type: String, required: true},
        reference_id: {type: String, required: false},
        read_status: {type: Boolean, required: false,default : false},
    },
    {
        timestamps: true
    }
);

export default model('user_notifications', UserNotificationSchema)
