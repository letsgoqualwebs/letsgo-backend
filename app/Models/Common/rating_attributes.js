import {model, Schema} from 'mongoose';

const RatingAttributeSchema = new Schema(
    {
        category: {type: String, required: true},
        name: {type: String, required: true},
    },
    {
        timestamps: true
    }
);

export default model('rating_attributes', RatingAttributeSchema)
