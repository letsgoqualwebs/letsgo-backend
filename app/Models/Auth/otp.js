import { Schema, model } from 'mongoose';

const OtpSchema = new Schema(
  {
    phone: { type: String, required: true },
    otp: { type: Number, required: true },
  },
  {
    timestamps: true
  }
);

export default model('Otp', OtpSchema)