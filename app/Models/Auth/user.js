import {model, Schema} from 'mongoose';

const UserSchema = new Schema(
    {
        first_name: {type: String, required: true},
        last_name: {type: String, required: true},
        dob: {type: String, required: false},
        email: {type: String, required: true},
        phone: {type: String, required: false},
        password: {type: String, required: true},
        business: {type: Object, required: false},
        role: {type: String, required: false},
        phone_verified_at: {type: String, required: false},
        email_verified_at: {type: String, required: false},
        marital_status: {type: Number, required: false},
        gender: {type: Number, required: false},
        profile_image: {type: String, required: false},
        google_id: {type: String, required: false},
        facebook_id: {type: String, required: false},
        apple_id: {type: String, required: false},
        notifications: {type: Object, required: false},
        firebase_tokens: {type: Array, required: false},
    },
    {
        timestamps: true
    }
);

export default model('User', UserSchema)
