import { Schema, model } from 'mongoose';

const EmailOtpSchema = new Schema(
  {
    email: { type: String, required: true },
    otp: { type: Number, required: true },
  },
  {
    timestamps: true
  }
);

export default model('email_otp', EmailOtpSchema)