import {model, Schema} from 'mongoose';

const ChatLineSchema = new Schema(
    {
        chat_thread_id: {type: String, required: true},
        thread: {type: Object, required: true},
        messages: {type: Array, required: true},
    },
    {
        timestamps: true
    }
);

export default model('chat_lines', ChatLineSchema)
