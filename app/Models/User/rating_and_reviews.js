import {model, Schema} from 'mongoose';

const RatingsAdnReviewSchema = new Schema(
    {
        experience_id: {type: String, required: true},
        user: {type: Object, required: true},
        review: {type: String, required: true},
        ratings: {type: Array, required: true},
    },
    {
        timestamps: true
    }
);

export default model('rating_and_reviews', RatingsAdnReviewSchema)
