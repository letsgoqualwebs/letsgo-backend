import {model, Schema} from 'mongoose';

const ChatThreadSchema = new Schema(
    {
        user_id: {type: String, required: true},
        provider_id: {type: String, required: true},
        user: {type: Object, required: true},
        provider: {type: Object, required: true},
        user_unread_count: {type: Number, required: false, default: 0},
        provider_unread_count: {type: Number, required: false, default: 0},
        last_message: {type: String, required: false, default: 1},
        last_timestamp: {type: String, required: false},
        status: {type: Boolean, required: true, default: 0},
    },
    {
        timestamps: true
    }
);

export default model('chat_threads', ChatThreadSchema)
