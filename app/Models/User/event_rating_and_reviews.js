import {model, Schema} from 'mongoose';

const EventRatingsAndReviewSchema = new Schema(
    {
        event_id: {type: String, required: true},
        user_id: {type: String, required: true},
        user: {type: Object, required: true},
        review: {type: String, required: true},
        rating: {type: Number, required: true},
    },
    {
        timestamps: true
    }
);

export default model('event_rating_and_reviews', EventRatingsAndReviewSchema)