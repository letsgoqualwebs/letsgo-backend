import {model, Schema} from 'mongoose';

const BookingSchema = new Schema(
    {
        user_id: {type: String, required: true},
        payment_card_id: {type: String, required: false},
        payment_reference: {type: String, required: false},
        payment_status: {type: Boolean, required: true, default : false},
        status: {type: Number, required: true, default : 1},
        sub_total: {type: String, required: true, default: 0},
        discount: {type: String, required: true, default: 0},
        tax: {type: String, required: true, default: 0},
        total_paid_amount: {type: String, required: true, default: 0},
        booking_details: {type: Object, required: true},
        order_items: {type: Array, required: true},
    },
    {
        timestamps: true
    }
);

export default model('Booking', BookingSchema)
