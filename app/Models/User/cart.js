import {model, Schema} from 'mongoose';

const CartSchema = new Schema(
    {
        cart_unique_id: {type: String, required: true},
        user_id: {type: String, required: false},
        sub_total: {type: Number, required: true, default:0 },
        tax: {type: Number, required: true ,default:0},
        discount: {type: Number, required: true,default:0},
        total_paid_amount: {type: Number, required: true,default:0},
        cart_items: {type: Array, required: false},
    },
    {
        timestamps: true
    }
);

export default model('Cart', CartSchema)
