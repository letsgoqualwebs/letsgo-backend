import {model, Schema} from 'mongoose';

const UserBillingAddressSchema = new Schema(
    {
        user_id: {type: String, required: true},
        billing_address : {type : Object,required : true}
    },
    {
        timestamps: true
    }
);

export default model('user_billing_address', UserBillingAddressSchema)
