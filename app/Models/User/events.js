import {model, Schema} from 'mongoose';

const EventSchema = new Schema(
    {
        user_id: {type: String, required: true},
        status: {type: Number, required: false, enum: [1, 2], default: 1},
        event_type: {type: String, required: true},
        event_title: {type: String, required: true},
        requirements: {type: Array, required: true},
        start_date: {type: Date, required: false},
        end_date: {type: Date, required: false},
        is_dates_flexible: {type: Boolean, required: false, enum: [true, false], default: false},
        start_time: {type: String, required: false},
        end_time: {type: String, required: false},
        is_times_flexible: {type: Boolean, required: false, enum: [true, false], default: false},
        number_of_guest: {type: Number, required: true},
        location: {type: String, required: true},
        budget: {type: String, required: false},
        is_budget_not_decided: {type: Boolean, required: false, enum: [true, false], default: false},
        activities: {type: Array, required: true},
        invitation_groups: {type: Array, required: true},
        proposals: {type: Array, required: true},
        requested_providers: {type: Array, required: false},
    },
    {
        timestamps: true
    }
);

export default model('Events', EventSchema)
