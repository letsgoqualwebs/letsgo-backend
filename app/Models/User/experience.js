import {model, Schema} from 'mongoose';

const ExperienceSchema = new Schema(
    {
        business_id: {type: String, required: true},
        is_published: {type: Number, required: false,enum: [0,1,2], default:0},
        status: {type: Boolean, required: false,enum: [true,false], default:true},
        exclusive: {type: Boolean, required: false,enum: [true,false], default:false},
        experience_details: {type: Object, required: false},
        experience_package: {type: Array, required: false},
        experience_availability: {type: Array, required: false},
        experience_schedule: {type: Array, required: false},
        experience_refund_policy: {type: Object, required: false},
        experience_gallery: {type: Array, required: false},
        experience_pages_completed: {type: Array, required: false},
    },
    {
        timestamps: true
    }
);

export default model('Experiences', ExperienceSchema)
