import { port } from './config/environment';
import graphqlServer from './app/graphql';
const bodyParser = require('body-parser');
import express from 'express';
import connectDB from './config/database';
import {graphqlUploadExpress} from 'graphql-upload';
const cors = require('cors')
const start = async () => {
  try {
    await connectDB();
    await graphqlServer.start();
    const app = express();
    app.use(bodyParser.json({ limit: '50mb' }));
    app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }));
    app.use(graphqlUploadExpress());
    app.use(cors());
    graphqlServer.applyMiddleware({
      app
    });
    await app.listen(port);
    console.log(`🚀  GraphQL server running at port: ${port}`);
  } catch (error) {
    console.log(error);
    console.log('Not able to run GraphQL server');
  }
};

start();
